﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace bestelsysteem_mayamaya
{
	class Database
	{
		private static string DEBUG = AppDomain.CurrentDomain.BaseDirectory; //debug folder
		private static string ROOT = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName; //proj root folder
		private static string connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + Database.ROOT + @"\Database\BestelsysteemDatabase.mdf;Integrated Security=True";

		private static SqlConnection con = new SqlConnection(connectionString);

		public static DataTable Request(SqlCommand query) {
			SqlDataAdapter adapter = new SqlDataAdapter();
			DataSet ds = new DataSet();

			try {
				con.Open();

				query.Connection = con;
				adapter.SelectCommand = query;
				adapter.Fill(ds, "result");

			} catch (Exception e) {
				LogError(e.Message, query.CommandText);
				//throw e;
				return null;

			} finally {
				con.Close();
			}

			return ds.Tables["result"];
		}

		//Updates and Deletes data in the database.
		public static bool Execute(SqlCommand query) {

			try {
				con.Open();

				query.Connection = con;
				query.ExecuteNonQuery();

			} catch (Exception e) {
				LogError(e.Message, query.CommandText);
				//throw e;
				return false;

			} finally {
				con.Close();
			}

			return true;
		}

		//Inserts data into the database and returns the new inserted id
		public static int Insert(SqlCommand query) {
			SqlDataAdapter adapter = new SqlDataAdapter();
			DataSet ds = new DataSet();

			query.CommandText = query.CommandText + "; Select @@Identity;";
			try {
				con.Open();

				query.Connection = con;
				adapter.SelectCommand = query;
				adapter.Fill(ds, "result");

			} catch (Exception e) {
				LogError(e.Message, query.CommandText);
				//throw e;
				return 0;

			} finally {
				con.Close();
			}

			if (ds.Tables["result"].Rows.Count == 0 || ds.Tables["result"].Rows[0][0] == DBNull.Value) {
				return 0;
			}
			
			return Convert.ToInt32(ds.Tables["result"].Rows[0][0]);
		}

		private static void LogError(string error, string query){
			Console.WriteLine("DATABASE_QUERY: " + query);
			Console.WriteLine("DATABASE_ERROR: " + error);
		}
	}
}
