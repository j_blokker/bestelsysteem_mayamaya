﻿namespace bestelsysteem_mayamaya
{
    partial class NieuwMedewerkerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.voornaamBox = new System.Windows.Forms.TextBox();
            this.achternaamBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.wachtwoordBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbBoxFunctie = new System.Windows.Forms.ComboBox();
            this.AnnulerenButton = new System.Windows.Forms.Button();
            this.btnAanmaken = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label1.Location = new System.Drawing.Point(57, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Voornaam:";
            // 
            // voornaamBox
            // 
            this.voornaamBox.Location = new System.Drawing.Point(166, 19);
            this.voornaamBox.Name = "voornaamBox";
            this.voornaamBox.Size = new System.Drawing.Size(220, 20);
            this.voornaamBox.TabIndex = 1;
            // 
            // achternaamBox
            // 
            this.achternaamBox.Location = new System.Drawing.Point(166, 50);
            this.achternaamBox.Name = "achternaamBox";
            this.achternaamBox.Size = new System.Drawing.Size(220, 20);
            this.achternaamBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label2.Location = new System.Drawing.Point(43, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "Achternaam:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label3.Location = new System.Drawing.Point(12, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(148, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "pin/wachtwoord:";
            // 
            // wachtwoordBox
            // 
            this.wachtwoordBox.Location = new System.Drawing.Point(166, 95);
            this.wachtwoordBox.Name = "wachtwoordBox";
            this.wachtwoordBox.Size = new System.Drawing.Size(220, 20);
            this.wachtwoordBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label4.Location = new System.Drawing.Point(82, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 24);
            this.label4.TabIndex = 6;
            this.label4.Text = "Functie:";
            // 
            // cbBoxFunctie
            // 
            this.cbBoxFunctie.FormattingEnabled = true;
            this.cbBoxFunctie.Location = new System.Drawing.Point(166, 144);
            this.cbBoxFunctie.Name = "cbBoxFunctie";
            this.cbBoxFunctie.Size = new System.Drawing.Size(220, 21);
            this.cbBoxFunctie.TabIndex = 7;
            // 
            // AnnulerenButton
            // 
            this.AnnulerenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.AnnulerenButton.Location = new System.Drawing.Point(154, 171);
            this.AnnulerenButton.Name = "AnnulerenButton";
            this.AnnulerenButton.Size = new System.Drawing.Size(113, 37);
            this.AnnulerenButton.TabIndex = 8;
            this.AnnulerenButton.Text = "Annuleren";
            this.AnnulerenButton.UseVisualStyleBackColor = true;
            this.AnnulerenButton.Click += new System.EventHandler(this.bttnAnnuleren_Click);
            // 
            // btnAanmaken
            // 
            this.btnAanmaken.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnAanmaken.Location = new System.Drawing.Point(273, 171);
            this.btnAanmaken.Name = "btnAanmaken";
            this.btnAanmaken.Size = new System.Drawing.Size(113, 37);
            this.btnAanmaken.TabIndex = 9;
            this.btnAanmaken.Text = "Aanmaken";
            this.btnAanmaken.UseVisualStyleBackColor = true;
            this.btnAanmaken.Click += new System.EventHandler(this.btnAanmaken_Click);
            // 
            // NieuwMedewerkerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 268);
            this.Controls.Add(this.btnAanmaken);
            this.Controls.Add(this.AnnulerenButton);
            this.Controls.Add(this.cbBoxFunctie);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.wachtwoordBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.achternaamBox);
            this.Controls.Add(this.voornaamBox);
            this.Controls.Add(this.label1);
            this.Name = "NieuwMedewerkerForm";
            this.Text = "NieuwMedewerkerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox voornaamBox;
        private System.Windows.Forms.TextBox achternaamBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox wachtwoordBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbBoxFunctie;
        private System.Windows.Forms.Button AnnulerenButton;
        private System.Windows.Forms.Button btnAanmaken;
    }
}