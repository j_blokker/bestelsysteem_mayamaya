﻿namespace bestelsysteem_mayamaya
{
    partial class ManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManagementForm));
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.lbltotaal = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.comBoTafels = new System.Windows.Forms.ComboBox();
			this.lvBestellingen = new System.Windows.Forms.ListView();
			this.lvBestellingenColNaam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestellingenColId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestelItemsInhoud = new System.Windows.Forms.ListView();
			this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestellingInhoudColMenuItemAantal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestellingInhoudColMenuItemNaam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestellingInhoudColPrijs = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.wijzigBttn = new System.Windows.Forms.Button();
			this.verwijderBttn = new System.Windows.Forms.Button();
			this.WijzigButton = new System.Windows.Forms.Button();
			this.verwijderButton = new System.Windows.Forms.Button();
			this.nieuwBttn = new System.Windows.Forms.Button();
			this.lvPersoneel = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.zoekbutton1 = new System.Windows.Forms.Button();
			this.zoekBox1 = new System.Windows.Forms.TextBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.bttnOpmerkingen = new System.Windows.Forms.Button();
			this.txtBoxOpmerking = new System.Windows.Forms.TextBox();
			this.Opmerkingtobutton = new System.Windows.Forms.Button();
			this.Opmerkingvanbutton = new System.Windows.Forms.Button();
			this.lvOpmerkingen = new System.Windows.Forms.ListView();
			this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.label3 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.lblIngelogdeMedewerker = new System.Windows.Forms.Label();
			this.btnLogout = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabControl1.Location = new System.Drawing.Point(2, 57);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(1253, 600);
			this.tabControl1.TabIndex = 10;
			// 
			// tabPage1
			// 
			this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
			this.tabPage1.Controls.Add(this.lbltotaal);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.comBoTafels);
			this.tabPage1.Controls.Add(this.lvBestellingen);
			this.tabPage1.Controls.Add(this.lvBestelItemsInhoud);
			this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabPage1.Location = new System.Drawing.Point(4, 33);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(1245, 563);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Facturen";
			// 
			// lbltotaal
			// 
			this.lbltotaal.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
			this.lbltotaal.Location = new System.Drawing.Point(761, 521);
			this.lbltotaal.Name = "lbltotaal";
			this.lbltotaal.Size = new System.Drawing.Size(475, 29);
			this.lbltotaal.TabIndex = 34;
			this.lbltotaal.Text = "Totaalbedrag:  €123,45";
			this.lbltotaal.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.label1.Location = new System.Drawing.Point(67, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(60, 24);
			this.label1.TabIndex = 33;
			this.label1.Text = "Toon:";
			// 
			// comBoTafels
			// 
			this.comBoTafels.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comBoTafels.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.comBoTafels.FormattingEnabled = true;
			this.comBoTafels.Location = new System.Drawing.Point(150, 19);
			this.comBoTafels.Name = "comBoTafels";
			this.comBoTafels.Size = new System.Drawing.Size(191, 32);
			this.comBoTafels.TabIndex = 32;
			this.comBoTafels.SelectedIndexChanged += new System.EventHandler(this.comBoTafels_SelectedIndexChanged);
			// 
			// lvBestellingen
			// 
			this.lvBestellingen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvBestellingenColNaam,
            this.lvBestellingenColId});
			this.lvBestellingen.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lvBestellingen.FullRowSelect = true;
			this.lvBestellingen.GridLines = true;
			this.lvBestellingen.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
			this.lvBestellingen.Location = new System.Drawing.Point(70, 57);
			this.lvBestellingen.MultiSelect = false;
			this.lvBestellingen.Name = "lvBestellingen";
			this.lvBestellingen.Size = new System.Drawing.Size(271, 451);
			this.lvBestellingen.TabIndex = 31;
			this.lvBestellingen.TileSize = new System.Drawing.Size(200, 40);
			this.lvBestellingen.UseCompatibleStateImageBehavior = false;
			this.lvBestellingen.View = System.Windows.Forms.View.Details;
			this.lvBestellingen.SelectedIndexChanged += new System.EventHandler(this.lvBestellingen_SelectedIndexChanged);
			// 
			// lvBestellingenColNaam
			// 
			this.lvBestellingenColNaam.Width = 265;
			// 
			// lvBestellingenColId
			// 
			this.lvBestellingenColId.Width = 0;
			// 
			// lvBestelItemsInhoud
			// 
			this.lvBestelItemsInhoud.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.lvBestellingInhoudColMenuItemAantal,
            this.lvBestellingInhoudColMenuItemNaam,
            this.lvBestellingInhoudColPrijs});
			this.lvBestelItemsInhoud.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lvBestelItemsInhoud.FullRowSelect = true;
			this.lvBestelItemsInhoud.GridLines = true;
			this.lvBestelItemsInhoud.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvBestelItemsInhoud.Location = new System.Drawing.Point(361, 19);
			this.lvBestelItemsInhoud.Name = "lvBestelItemsInhoud";
			this.lvBestelItemsInhoud.Size = new System.Drawing.Size(875, 489);
			this.lvBestelItemsInhoud.TabIndex = 30;
			this.lvBestelItemsInhoud.TileSize = new System.Drawing.Size(212, 40);
			this.lvBestelItemsInhoud.UseCompatibleStateImageBehavior = false;
			this.lvBestelItemsInhoud.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Width = 0;
			// 
			// lvBestellingInhoudColMenuItemAantal
			// 
			this.lvBestellingInhoudColMenuItemAantal.Text = "Aantal";
			this.lvBestellingInhoudColMenuItemAantal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.lvBestellingInhoudColMenuItemAantal.Width = 90;
			// 
			// lvBestellingInhoudColMenuItemNaam
			// 
			this.lvBestellingInhoudColMenuItemNaam.Tag = "";
			this.lvBestellingInhoudColMenuItemNaam.Text = "Naam";
			this.lvBestellingInhoudColMenuItemNaam.Width = 560;
			// 
			// lvBestellingInhoudColPrijs
			// 
			this.lvBestellingInhoudColPrijs.Text = "Prijs";
			this.lvBestellingInhoudColPrijs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.lvBestellingInhoudColPrijs.Width = 220;
			// 
			// tabPage2
			// 
			this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.tabPage2.Controls.Add(this.wijzigBttn);
			this.tabPage2.Controls.Add(this.verwijderBttn);
			this.tabPage2.Controls.Add(this.WijzigButton);
			this.tabPage2.Controls.Add(this.verwijderButton);
			this.tabPage2.Controls.Add(this.nieuwBttn);
			this.tabPage2.Controls.Add(this.lvPersoneel);
			this.tabPage2.Controls.Add(this.zoekbutton1);
			this.tabPage2.Controls.Add(this.zoekBox1);
			this.tabPage2.Location = new System.Drawing.Point(4, 33);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(1245, 563);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Personeel";
			// 
			// wijzigBttn
			// 
			this.wijzigBttn.Location = new System.Drawing.Point(31, 520);
			this.wijzigBttn.Name = "wijzigBttn";
			this.wijzigBttn.Size = new System.Drawing.Size(113, 37);
			this.wijzigBttn.TabIndex = 10;
			this.wijzigBttn.Text = "Wijzig";
			this.wijzigBttn.UseVisualStyleBackColor = true;
			this.wijzigBttn.Click += new System.EventHandler(this.wijzigBttn_Click);
			// 
			// verwijderBttn
			// 
			this.verwijderBttn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.verwijderBttn.Location = new System.Drawing.Point(1105, 520);
			this.verwijderBttn.Name = "verwijderBttn";
			this.verwijderBttn.Size = new System.Drawing.Size(113, 37);
			this.verwijderBttn.TabIndex = 9;
			this.verwijderBttn.Text = "Verwijder";
			this.verwijderBttn.UseVisualStyleBackColor = true;
			this.verwijderBttn.Click += new System.EventHandler(this.verwijderBttn_Click);
			// 
			// WijzigButton
			// 
			this.WijzigButton.Location = new System.Drawing.Point(1103, 656);
			this.WijzigButton.Name = "WijzigButton";
			this.WijzigButton.Size = new System.Drawing.Size(115, 32);
			this.WijzigButton.TabIndex = 8;
			this.WijzigButton.Text = "Wijzig";
			this.WijzigButton.UseVisualStyleBackColor = true;
			// 
			// verwijderButton
			// 
			this.verwijderButton.Location = new System.Drawing.Point(31, 656);
			this.verwijderButton.Name = "verwijderButton";
			this.verwijderButton.Size = new System.Drawing.Size(115, 32);
			this.verwijderButton.TabIndex = 7;
			this.verwijderButton.Text = "Verwijder";
			this.verwijderButton.UseVisualStyleBackColor = true;
			// 
			// nieuwBttn
			// 
			this.nieuwBttn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.nieuwBttn.Location = new System.Drawing.Point(1105, 18);
			this.nieuwBttn.Name = "nieuwBttn";
			this.nieuwBttn.Size = new System.Drawing.Size(113, 37);
			this.nieuwBttn.TabIndex = 6;
			this.nieuwBttn.Text = "Nieuw";
			this.nieuwBttn.UseVisualStyleBackColor = true;
			this.nieuwBttn.Click += new System.EventHandler(this.nieuwBttn_Click);
			// 
			// lvPersoneel
			// 
			this.lvPersoneel.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
			this.lvPersoneel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.lvPersoneel.FullRowSelect = true;
			this.lvPersoneel.GridLines = true;
			this.lvPersoneel.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvPersoneel.Location = new System.Drawing.Point(31, 61);
			this.lvPersoneel.Name = "lvPersoneel";
			this.lvPersoneel.Scrollable = false;
			this.lvPersoneel.Size = new System.Drawing.Size(1187, 453);
			this.lvPersoneel.TabIndex = 5;
			this.lvPersoneel.TileSize = new System.Drawing.Size(212, 40);
			this.lvPersoneel.UseCompatibleStateImageBehavior = false;
			this.lvPersoneel.View = System.Windows.Forms.View.Details;
			this.lvPersoneel.SelectedIndexChanged += new System.EventHandler(this.lvPersoneel_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "ID";
			this.columnHeader1.Width = 188;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Voornaam";
			this.columnHeader2.Width = 250;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Achternaam";
			this.columnHeader3.Width = 333;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Functie";
			this.columnHeader4.Width = 415;
			// 
			// zoekbutton1
			// 
			this.zoekbutton1.Location = new System.Drawing.Point(171, 26);
			this.zoekbutton1.Name = "zoekbutton1";
			this.zoekbutton1.Size = new System.Drawing.Size(51, 29);
			this.zoekbutton1.TabIndex = 1;
			this.zoekbutton1.Text = "\"";
			this.zoekbutton1.UseVisualStyleBackColor = true;
			// 
			// zoekBox1
			// 
			this.zoekBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.zoekBox1.Location = new System.Drawing.Point(31, 26);
			this.zoekBox1.Name = "zoekBox1";
			this.zoekBox1.Size = new System.Drawing.Size(134, 29);
			this.zoekBox1.TabIndex = 0;
			this.zoekBox1.Text = "Zoek";
			// 
			// tabPage3
			// 
			this.tabPage3.BackColor = System.Drawing.Color.WhiteSmoke;
			this.tabPage3.Controls.Add(this.bttnOpmerkingen);
			this.tabPage3.Controls.Add(this.txtBoxOpmerking);
			this.tabPage3.Controls.Add(this.Opmerkingtobutton);
			this.tabPage3.Controls.Add(this.Opmerkingvanbutton);
			this.tabPage3.Controls.Add(this.lvOpmerkingen);
			this.tabPage3.Controls.Add(this.label3);
			this.tabPage3.Controls.Add(this.textBox1);
			this.tabPage3.Controls.Add(this.label4);
			this.tabPage3.Controls.Add(this.textBox2);
			this.tabPage3.Location = new System.Drawing.Point(4, 33);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(1245, 563);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Opmerkingen";
			// 
			// bttnOpmerkingen
			// 
			this.bttnOpmerkingen.Location = new System.Drawing.Point(1085, 507);
			this.bttnOpmerkingen.Name = "bttnOpmerkingen";
			this.bttnOpmerkingen.Size = new System.Drawing.Size(145, 39);
			this.bttnOpmerkingen.TabIndex = 19;
			this.bttnOpmerkingen.Text = "Aanpassen";
			this.bttnOpmerkingen.UseVisualStyleBackColor = true;
			this.bttnOpmerkingen.Click += new System.EventHandler(this.bttnOpmerkingen_Click);
			// 
			// txtBoxOpmerking
			// 
			this.txtBoxOpmerking.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtBoxOpmerking.Location = new System.Drawing.Point(361, 33);
			this.txtBoxOpmerking.Multiline = true;
			this.txtBoxOpmerking.Name = "txtBoxOpmerking";
			this.txtBoxOpmerking.Size = new System.Drawing.Size(881, 464);
			this.txtBoxOpmerking.TabIndex = 18;
			// 
			// Opmerkingtobutton
			// 
			this.Opmerkingtobutton.Location = new System.Drawing.Point(277, 83);
			this.Opmerkingtobutton.Name = "Opmerkingtobutton";
			this.Opmerkingtobutton.Size = new System.Drawing.Size(64, 29);
			this.Opmerkingtobutton.TabIndex = 17;
			this.Opmerkingtobutton.Text = "\"";
			this.Opmerkingtobutton.UseVisualStyleBackColor = true;
			// 
			// Opmerkingvanbutton
			// 
			this.Opmerkingvanbutton.Location = new System.Drawing.Point(277, 33);
			this.Opmerkingvanbutton.Name = "Opmerkingvanbutton";
			this.Opmerkingvanbutton.Size = new System.Drawing.Size(64, 29);
			this.Opmerkingvanbutton.TabIndex = 16;
			this.Opmerkingvanbutton.Text = "\"";
			this.Opmerkingvanbutton.UseVisualStyleBackColor = true;
			// 
			// lvOpmerkingen
			// 
			this.lvOpmerkingen.Activation = System.Windows.Forms.ItemActivation.OneClick;
			this.lvOpmerkingen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5});
			this.lvOpmerkingen.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.lvOpmerkingen.FullRowSelect = true;
			this.lvOpmerkingen.GridLines = true;
			this.lvOpmerkingen.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvOpmerkingen.HideSelection = false;
			this.lvOpmerkingen.Location = new System.Drawing.Point(36, 128);
			this.lvOpmerkingen.Name = "lvOpmerkingen";
			this.lvOpmerkingen.Scrollable = false;
			this.lvOpmerkingen.Size = new System.Drawing.Size(306, 429);
			this.lvOpmerkingen.TabIndex = 15;
			this.lvOpmerkingen.TileSize = new System.Drawing.Size(212, 40);
			this.lvOpmerkingen.UseCompatibleStateImageBehavior = false;
			this.lvOpmerkingen.View = System.Windows.Forms.View.Details;
			this.lvOpmerkingen.SelectedIndexChanged += new System.EventHandler(this.lvOpmerkingen_SelectedIndexChanged);
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "ID";
			this.columnHeader5.Width = 307;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.label3.Location = new System.Drawing.Point(39, 86);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(42, 24);
			this.label3.TabIndex = 13;
			this.label3.Text = "Tot:";
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.textBox1.Location = new System.Drawing.Point(120, 33);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(145, 29);
			this.textBox1.TabIndex = 10;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.label4.Location = new System.Drawing.Point(32, 36);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(49, 24);
			this.label4.TabIndex = 12;
			this.label4.Text = "Van:";
			// 
			// textBox2
			// 
			this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.textBox2.Location = new System.Drawing.Point(120, 83);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(145, 29);
			this.textBox2.TabIndex = 11;
			// 
			// lblIngelogdeMedewerker
			// 
			this.lblIngelogdeMedewerker.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblIngelogdeMedewerker.Location = new System.Drawing.Point(751, 31);
			this.lblIngelogdeMedewerker.Name = "lblIngelogdeMedewerker";
			this.lblIngelogdeMedewerker.Size = new System.Drawing.Size(450, 24);
			this.lblIngelogdeMedewerker.TabIndex = 14;
			this.lblIngelogdeMedewerker.Text = "[Medewerker]";
			this.lblIngelogdeMedewerker.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// btnLogout
			// 
			this.btnLogout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLogout.BackgroundImage")));
			this.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnLogout.FlatAppearance.BorderSize = 0;
			this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.btnLogout.Location = new System.Drawing.Point(1210, 21);
			this.btnLogout.Margin = new System.Windows.Forms.Padding(0);
			this.btnLogout.Name = "btnLogout";
			this.btnLogout.Size = new System.Drawing.Size(45, 45);
			this.btnLogout.TabIndex = 15;
			this.btnLogout.UseVisualStyleBackColor = true;
			this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click_1);
			// 
			// ManagementForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1264, 661);
			this.ControlBox = false;
			this.Controls.Add(this.btnLogout);
			this.Controls.Add(this.lblIngelogdeMedewerker);
			this.Controls.Add(this.tabControl1);
			this.Name = "ManagementForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ManagementForm";
			this.Shown += new System.EventHandler(this.ManagementForm_Shown);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.tabPage3.ResumeLayout(false);
			this.tabPage3.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button WijzigButton;
        private System.Windows.Forms.Button verwijderButton;
        private System.Windows.Forms.Button nieuwBttn;
        private System.Windows.Forms.ListView lvPersoneel;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button zoekbutton1;
        private System.Windows.Forms.TextBox zoekBox1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ListView lvOpmerkingen;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button Opmerkingtobutton;
        private System.Windows.Forms.Button Opmerkingvanbutton;
        private System.Windows.Forms.Button verwijderBttn;
        private System.Windows.Forms.Button wijzigBttn;
        private System.Windows.Forms.TextBox txtBoxOpmerking;
        private System.Windows.Forms.Button bttnOpmerkingen;
        private System.Windows.Forms.Label lblIngelogdeMedewerker;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.ListView lvBestelItemsInhoud;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader lvBestellingInhoudColMenuItemAantal;
        private System.Windows.Forms.ColumnHeader lvBestellingInhoudColMenuItemNaam;
        private System.Windows.Forms.ColumnHeader lvBestellingInhoudColPrijs;
        private System.Windows.Forms.ListView lvBestellingen;
        private System.Windows.Forms.ColumnHeader lvBestellingenColNaam;
        private System.Windows.Forms.ColumnHeader lvBestellingenColId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comBoTafels;
        private System.Windows.Forms.Label lbltotaal;
    }
}