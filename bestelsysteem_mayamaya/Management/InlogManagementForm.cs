﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
    public partial class InlogManagementForm : Form
    {
        
        BestelSysteem bestelSysteem;
        HoofdmenuForm mainForm;


        public InlogManagementForm(HoofdmenuForm mainForm, BestelSysteem bestelSysteem)
        {
           
            InitializeComponent();

            this.bestelSysteem = bestelSysteem;
            this.mainForm = mainForm;

        }

        
        private void bttnAnnuleren_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void bttnLogin_Click(object sender, EventArgs e)
        {
			//inloggen als manager via een methode in bestelsysteem
            bestelSysteem.LoginManager(gebruikersNaamBox.Text, wachtwoordBox.Text);
            
            //inloggen gelukt -> open managementForm.
            if (bestelSysteem.IngelogdeWerknemer != null)
            {
                ManagementForm ManagementForm = new ManagementForm(bestelSysteem);
                ManagementForm.Show();
                ManagementForm.FormClosing += delegate { mainForm.Show(); };
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            // niet gelukt geef fouutmelding en probeer het opnieuw
            else 
            {
                MessageBox.Show("Onjuiste inloggevens, probeer het nogmaals");
                wachtwoordBox.Text = "";

            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
