﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
    public partial class WijzigWerknemerForm : Form
    {
        BestelSysteem bestelSysteem;
        int personeelsID;
       
        public WijzigWerknemerForm(int personeelsId, BestelSysteem bestelSysteem)
        {
            
            this.bestelSysteem = bestelSysteem;
            InitializeComponent();
            personeelsID = personeelsId;
            vullenvelden();
            
        }
        
        //Vullen van medewerker specifieke data (voornaam, Achternaam, Pincode)
        private void vullenvelden()
        {
            Werknemer werknemer = bestelSysteem.ZoekWerknemerOpId(personeelsID);

            voornaamBox.Text = werknemer.Voornaam;
            achternaamBox.Text = werknemer.Achternaam;
            wachtwoordBox.Text = werknemer.Pincode;

            foreach (Functie func in Enum.GetValues(typeof(Functie)))
            {
                cbBoxFunctie.Items.Add(func);
            }

        }

        //Annuleren en terug gaan naar ManagementForm
        private void AnnulerenButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //wijzigen van de werknemer
        private void bttnWijzigen_Click(object sender, EventArgs e)
        {
            //opslaan data van data uit de velden in de specifieke werknemer in de list
            Werknemer werknemer = bestelSysteem.ZoekWerknemerOpId(personeelsID);
            werknemer.Voornaam = voornaamBox.Text;
            werknemer.Achternaam = achternaamBox.Text;
            werknemer.Pincode = wachtwoordBox.Text;
            werknemer.Functie = (Functie)cbBoxFunctie.SelectedItem;

            //Aanroepen WijzigWerknemerForm en result terug krijgen 
            bool result = bestelSysteem.WijzigWerknemer(werknemer);
            
            // niet gelukt -> error message
            if (result == false)
            {
                MessageBox.Show("Het wijzigen van de werknemer is mislukt, probeer het nogmaals"); 
            }
            else
            {
                this.DialogResult = DialogResult.OK; 
                this.Close();
                
            }
           
        }
    }
}
