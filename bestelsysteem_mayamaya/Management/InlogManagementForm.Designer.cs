﻿namespace bestelsysteem_mayamaya
{
	partial class InlogManagementForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.bttnAnnuleren = new System.Windows.Forms.Button();
			this.bttnLogin = new System.Windows.Forms.Button();
			this.lblwachtwoord = new System.Windows.Forms.Label();
			this.wachtwoordBox = new System.Windows.Forms.TextBox();
			this.lblGebruikersnaam = new System.Windows.Forms.Label();
			this.gebruikersNaamBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// bttnAnnuleren
			// 
			this.bttnAnnuleren.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.bttnAnnuleren.Location = new System.Drawing.Point(154, 179);
			this.bttnAnnuleren.Name = "bttnAnnuleren";
			this.bttnAnnuleren.Size = new System.Drawing.Size(131, 37);
			this.bttnAnnuleren.TabIndex = 8;
			this.bttnAnnuleren.Text = "Annuleren";
			this.bttnAnnuleren.UseVisualStyleBackColor = true;
			// 
			// bttnLogin
			// 
			this.bttnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.bttnLogin.Location = new System.Drawing.Point(305, 179);
			this.bttnLogin.Name = "bttnLogin";
			this.bttnLogin.Size = new System.Drawing.Size(131, 37);
			this.bttnLogin.TabIndex = 9;
			this.bttnLogin.Text = "Login";
			this.bttnLogin.UseVisualStyleBackColor = true;
			this.bttnLogin.Click += new System.EventHandler(this.bttnLogin_Click);
			// 
			// lblwachtwoord
			// 
			this.lblwachtwoord.AutoSize = true;
			this.lblwachtwoord.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblwachtwoord.Location = new System.Drawing.Point(133, 126);
			this.lblwachtwoord.Name = "lblwachtwoord";
			this.lblwachtwoord.Size = new System.Drawing.Size(121, 24);
			this.lblwachtwoord.TabIndex = 7;
			this.lblwachtwoord.Text = "Wachtwoord:";
			// 
			// wachtwoordBox
			// 
			this.wachtwoordBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.wachtwoordBox.Location = new System.Drawing.Point(265, 123);
			this.wachtwoordBox.MaxLength = 255;
			this.wachtwoordBox.Name = "wachtwoordBox";
			this.wachtwoordBox.PasswordChar = '*';
			this.wachtwoordBox.Size = new System.Drawing.Size(171, 29);
			this.wachtwoordBox.TabIndex = 1;
			// 
			// lblGebruikersnaam
			// 
			this.lblGebruikersnaam.AutoSize = true;
			this.lblGebruikersnaam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblGebruikersnaam.Location = new System.Drawing.Point(100, 81);
			this.lblGebruikersnaam.Name = "lblGebruikersnaam";
			this.lblGebruikersnaam.Size = new System.Drawing.Size(154, 24);
			this.lblGebruikersnaam.TabIndex = 10;
			this.lblGebruikersnaam.Text = "Gebruikersnaam:";
			this.lblGebruikersnaam.Click += new System.EventHandler(this.label1_Click);
			// 
			// gebruikersNaamBox
			// 
			this.gebruikersNaamBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.gebruikersNaamBox.Location = new System.Drawing.Point(265, 81);
			this.gebruikersNaamBox.MaxLength = 255;
			this.gebruikersNaamBox.Name = "gebruikersNaamBox";
			this.gebruikersNaamBox.Size = new System.Drawing.Size(171, 29);
			this.gebruikersNaamBox.TabIndex = 0;
			// 
			// InlogManagementForm
			// 
			this.AcceptButton = this.bttnLogin;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.bttnAnnuleren;
			this.ClientSize = new System.Drawing.Size(624, 345);
			this.Controls.Add(this.gebruikersNaamBox);
			this.Controls.Add(this.lblGebruikersnaam);
			this.Controls.Add(this.bttnAnnuleren);
			this.Controls.Add(this.bttnLogin);
			this.Controls.Add(this.lblwachtwoord);
			this.Controls.Add(this.wachtwoordBox);
			this.Name = "InlogManagementForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "InlogManagementForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button bttnAnnuleren;
		private System.Windows.Forms.Button bttnLogin;
		private System.Windows.Forms.Label lblwachtwoord;
		private System.Windows.Forms.TextBox wachtwoordBox;
		private System.Windows.Forms.Label lblGebruikersnaam;
		private System.Windows.Forms.TextBox gebruikersNaamBox;

	}
}