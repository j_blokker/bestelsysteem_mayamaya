﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
    public partial class VerwijderWerknemerForm : Form
    {
        
        public VerwijderWerknemerForm()
        {
            InitializeComponent();
           
        }
        //Annuleren en terug gaan naar ManagementForm
        private void bttnAnnuleren_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.Cancel;
        }
        
        //Terug geven van de "go ahead" aan ManagementForm
        private void verwijderWerknemerKnop_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
