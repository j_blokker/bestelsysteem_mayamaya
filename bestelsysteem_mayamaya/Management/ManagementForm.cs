﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace bestelsysteem_mayamaya
{
    public partial class ManagementForm : Form
    {
        BestelSysteem bestelSysteem;
        int personeelsId = 0;

        public ManagementForm(BestelSysteem bestelSysteem)
        {
            InitializeComponent();

            this.bestelSysteem = bestelSysteem;
            WeergevenWerknemers();
            WeergevenOpmerkingen();
			WeergevenFacturen();

			//Vullen van dropdownbalk met tafels
			comBoTafels.Items.Add("Alles");
			foreach (Tafel tafel in bestelSysteem.Tafels) {
				comBoTafels.Items.Add(tafel);
			}
			comBoTafels.SelectedIndex = 0;

			lbltotaal.Text = "";

        }
        
        //Combobox tafels in facturentab 
        private void comBoTafels_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            List<Bestelling> gefilterdeBestellingen = new List<Bestelling>();
			List<Bestelling> bestellingen = bestelSysteem.OphalenDagBestellingen();

			lbltotaal.Text = "";
			lvBestellingen.Items.Clear();
			lvBestelItemsInhoud.Items.Clear();

            //Wanneer geselecteerde optie 'alles' betreft toon alles.
            //Anders alleen de bestellingen weergeven voor de geselecteerde tafel
			if (comBoTafels.SelectedItem is string) {
				foreach (Bestelling bestelling in bestellingen) {
					gefilterdeBestellingen.Add(bestelling);
				}
			}
			else {
				Tafel geselecteerdeTafel = (Tafel)comBoTafels.SelectedItem;

				//Filteren van bestellingen op tafelId
				foreach (Bestelling bestelling in bestellingen) {
					if (bestelling.TafelId == geselecteerdeTafel.Id && bestelling.Afgerond) {
						gefilterdeBestellingen.Add(bestelling);
					}
				}
			}

			//Vullen van bestellingoverzicht met de gefilterde bestellingen
			foreach (Bestelling gefilterdeBestelling in gefilterdeBestellingen) {
				ListViewItem lvItem = new ListViewItem("Bestelling " + gefilterdeBestelling.Id);
				lvItem.SubItems.Add(gefilterdeBestelling.Id.ToString());

				lvBestellingen.Items.Add(lvItem);
			}
        }


        #region Listview vullen met Data (opmerkingen, facturen en personeel)

        //weergeven van de Werknemers in het personeeltabje
        private void WeergevenWerknemers()
        {
            List<Werknemer> werknemers = bestelSysteem.Werknemers;

            //listview leegmaken
            lvPersoneel.Items.Clear();

            //Loopen door Werknemers en weergeven in de ListView!
            foreach (Werknemer werknemer in werknemers)
            {
                //ListViewItem aanmaken en toevoegen
                ListViewItem lvItem = new ListViewItem(werknemer.Id.ToString());
                lvItem.SubItems.Add(werknemer.Voornaam);
                lvItem.SubItems.Add(werknemer.Achternaam);
                lvItem.SubItems.Add(werknemer.Functie.ToString());

                lvPersoneel.Items.Add(lvItem);
            }
        }

        //Weergeven van Opmerkingen
        private void WeergevenOpmerkingen()
        {
            //list bestellingen gelijkstellen met list in bestelsysteem
            List<Bestelling> bestellingen = bestelSysteem.Bestellingen;

            //listview leegmaken
            lvOpmerkingen.Items.Clear();

            //loopen door bestellingen en plaaten in listview
            foreach (Bestelling bestelling in bestellingen)
            {
                //ListViewItem aanmaken en toevoegen
                ListViewItem lvItem = new ListViewItem(bestelling.Id.ToString());

                lvOpmerkingen.Items.Add(lvItem);
            }
        }

        //Weergeven van Opmerkingen
        private void WeergevenFacturen()
        {
            //loopen door bestellingen en plaatsen in listview
            foreach (Bestelling bestelling in bestelSysteem.Bestellingen)
            {
                ListViewItem lvItem = new ListViewItem("Factuur " + bestelling.Id);
                lvItem.SubItems.Add(bestelling.Id.ToString());

                lvBestellingen.Items.Add(lvItem);
            }
        }


        #endregion

        #region events (button clicks)
       
        //wijzigen van werknemer via nieuwe form WijzigWerknemerForm
        private void wijzigBttn_Click(object sender, EventArgs e)
        {
            if (personeelsId == 0)
            {
                return;
            }

            //Aanroepen nieuwe form
            WijzigWerknemerForm WijzigWerknemerForm = new WijzigWerknemerForm(personeelsId, bestelSysteem);

            //Dialogresult teruggeven na gelukte actie
            DialogResult result = WijzigWerknemerForm.ShowDialog();

            //Als result OK is listview leegmaken en weer vullen
            if (result == DialogResult.OK)
            {
                
                WeergevenWerknemers();
            }
        }

        //Nieuwe werknemer toevoegen via nieuwe form NieuwMedewerkerForm
        private void nieuwBttn_Click(object sender, EventArgs e)
        {
            //Aanroepen nieuwe form
            NieuwMedewerkerForm NieuwMedewerkerForm = new NieuwMedewerkerForm(bestelSysteem);

            //Dialogresult teruggeven na gelukte actie
            DialogResult result = NieuwMedewerkerForm.ShowDialog();

            //Als result OK is listview leegmaken en weer vullen
            if (result == DialogResult.OK)
            {
               
                WeergevenWerknemers();
            }
        }

        //Werknemer verwijderen via nieuwe form VerwijderWerknemerForm
        private void verwijderBttn_Click(object sender, EventArgs e)
        {
            //Aanroepen nieuwe form
            VerwijderWerknemerForm VerwijderWerknemerForm = new VerwijderWerknemerForm();

            //Dialogresult teruggeven na gelukte actie
            DialogResult result = VerwijderWerknemerForm.ShowDialog();

            if (result == DialogResult.OK)
            {

                //Als result OK is listview leegmaken en weer vullen
                bool resultaat = bestelSysteem.VerwijderWerknemer(personeelsId);
                if (resultaat == false)
                {
                    MessageBox.Show("Werknemer verwijderen mislukt, probeer nogmaals.");
                }
                else
                {
                    
                    WeergevenWerknemers();

                }
            }
        }

        // Opmerking aanpassen
        private void bttnOpmerkingen_Click(object sender, EventArgs e)
        {
            //Kijken of er een id geselecteerd is zo niet return
            if (lvOpmerkingen.SelectedItems.Count == 0)
            {
                return;
            }

            //BestellingsId vullen met de ID die geselecteerd is 
            int bestellingId = 0;
            bestellingId = Convert.ToInt32(lvOpmerkingen.SelectedItems[0].SubItems[0].Text);

            //Aangepaste opmerking opslaan in txtOpmerking 
            string txtOpmerking = txtBoxOpmerking.Text;

            //Methode OpmerkingAanpassen in bestelsysteem aanroepen en ID en Opmerking meegeven, 
            //Methode geeft bool terug
            bool result = bestelSysteem.OpmerkingAanpassen(bestellingId, txtOpmerking);

            //Als de uitvoer van de methode niet is gelukt -> Foutmelding
            if (result == false)
            {
                MessageBox.Show("Er is een fout opgetreden, probeer het nogmaals.");
            }

            WeergevenOpmerkingen();
        }

        // uitloggen
        private void btnLogout_Click_1(object sender, EventArgs e)
        {
            bestelSysteem.Logout();
            this.Close();
        }

        #endregion

        #region Listview_IndexChanged events ( als er op een bepaalde listview item word geklikt) 
        private void lvBestellingen_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Reset
            lvBestelItemsInhoud.Items.Clear();


            if (lvBestellingen.SelectedItems.Count == 0)
            {
                return;
            }

            double totaalPrijs = 0;
            lbltotaal.Text = "";

            //Ophalen bestelling via geselecteerde bestelling
            int bestellingId = Int32.Parse(lvBestellingen.SelectedItems[0].SubItems[1].Text);
            Bestelling bestelling = bestelSysteem.ZoekBestellingOpBestellingId(bestellingId);
            List<BestelItem> besteldeItems = bestelSysteem.BestelItems;

            //Invullen van detailview

            foreach (BestelItem bItem in besteldeItems)
            {
                if (bItem.BestellingId == bestelling.Id)
                {
                    MenuItem mItem = bestelSysteem.ZoekMenuItemOpMenuItemId(bItem.MenuItemId);

                    ListViewItem lvItem = new ListViewItem();
                    lvItem.SubItems.Add(bItem.Aantal.ToString() + "x");
                    lvItem.SubItems.Add(mItem.Naam);
                    lvItem.SubItems.Add((mItem.Prijs * bItem.Aantal).ToString("C", new CultureInfo("nl-NL")));
                    double subprijs = mItem.Prijs * bItem.Aantal;
                    lvBestelItemsInhoud.Items.Add(lvItem);

                    totaalPrijs = totaalPrijs + subprijs;

                    lbltotaal.Text = "Totaalbedrag: " + totaalPrijs.ToString("c", new CultureInfo("nl-NL"));
                }
            }
        }


        private void lvOpmerkingen_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvOpmerkingen.SelectedItems.Count == 0)
            {
                return;
            }
            int bestellingId = Convert.ToInt32(lvOpmerkingen.SelectedItems[0].SubItems[0].Text);
            Bestelling bestelling = bestelSysteem.ZoekBestellingOpBestellingId(bestellingId);
            txtBoxOpmerking.Text = bestelling.Opmerking;
        }

        private void lvPersoneel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvPersoneel.SelectedItems.Count == 0)
            {
                return;
            }
            personeelsId = Convert.ToInt32(lvPersoneel.SelectedItems[0].SubItems[0].Text);
        }


        #endregion

        //werknemer naam laten zien in label
        private void ManagementForm_Shown(object sender, EventArgs e) {
			lblIngelogdeMedewerker.Text = bestelSysteem.IngelogdeWerknemer.Voornaam + " " + bestelSysteem.IngelogdeWerknemer.Achternaam;
		}
    }
}

