﻿namespace bestelsysteem_mayamaya
{
    partial class WijzigWerknemerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AnnulerenButton = new System.Windows.Forms.Button();
            this.bttnWijzigen = new System.Windows.Forms.Button();
            this.cbBoxFunctie = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.wachtwoordBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.achternaamBox = new System.Windows.Forms.TextBox();
            this.voornaamBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AnnulerenButton
            // 
            this.AnnulerenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.AnnulerenButton.Location = new System.Drawing.Point(123, 221);
            this.AnnulerenButton.Name = "AnnulerenButton";
            this.AnnulerenButton.Size = new System.Drawing.Size(113, 37);
            this.AnnulerenButton.TabIndex = 18;
            this.AnnulerenButton.Text = "Annuleren";
            this.AnnulerenButton.UseVisualStyleBackColor = true;
            this.AnnulerenButton.Click += new System.EventHandler(this.AnnulerenButton_Click);
            // 
            // bttnWijzigen
            // 
            this.bttnWijzigen.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.bttnWijzigen.Location = new System.Drawing.Point(242, 221);
            this.bttnWijzigen.Name = "bttnWijzigen";
            this.bttnWijzigen.Size = new System.Drawing.Size(113, 37);
            this.bttnWijzigen.TabIndex = 19;
            this.bttnWijzigen.Text = "Wijzigen";
            this.bttnWijzigen.UseVisualStyleBackColor = true;
            this.bttnWijzigen.Click += new System.EventHandler(this.bttnWijzigen_Click);
            // 
            // cbBoxFunctie
            // 
            this.cbBoxFunctie.FormattingEnabled = true;
            this.cbBoxFunctie.Location = new System.Drawing.Point(135, 120);
            this.cbBoxFunctie.Name = "cbBoxFunctie";
            this.cbBoxFunctie.Size = new System.Drawing.Size(220, 21);
            this.cbBoxFunctie.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label4.Location = new System.Drawing.Point(56, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 24);
            this.label4.TabIndex = 16;
            this.label4.Text = "Functie:";
            // 
            // wachtwoordBox
            // 
            this.wachtwoordBox.Location = new System.Drawing.Point(135, 85);
            this.wachtwoordBox.Name = "wachtwoordBox";
            this.wachtwoordBox.Size = new System.Drawing.Size(220, 20);
            this.wachtwoordBox.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label3.Location = new System.Drawing.Point(13, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 24);
            this.label3.TabIndex = 14;
            this.label3.Text = "Wachtwoord:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label2.Location = new System.Drawing.Point(17, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 24);
            this.label2.TabIndex = 13;
            this.label2.Text = "Achternaam:";
            // 
            // achternaamBox
            // 
            this.achternaamBox.Location = new System.Drawing.Point(135, 48);
            this.achternaamBox.Name = "achternaamBox";
            this.achternaamBox.Size = new System.Drawing.Size(220, 20);
            this.achternaamBox.TabIndex = 12;
            // 
            // voornaamBox
            // 
            this.voornaamBox.Location = new System.Drawing.Point(135, 16);
            this.voornaamBox.Name = "voornaamBox";
            this.voornaamBox.Size = new System.Drawing.Size(220, 20);
            this.voornaamBox.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label1.Location = new System.Drawing.Point(31, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 24);
            this.label1.TabIndex = 10;
            this.label1.Text = "Voornaam:";
            // 
            // WijzigWerknemerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 268);
            this.Controls.Add(this.AnnulerenButton);
            this.Controls.Add(this.bttnWijzigen);
            this.Controls.Add(this.cbBoxFunctie);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.wachtwoordBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.achternaamBox);
            this.Controls.Add(this.voornaamBox);
            this.Controls.Add(this.label1);
            this.Name = "WijzigWerknemerForm";
            this.Text = "WijzigWerknemer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AnnulerenButton;
        private System.Windows.Forms.Button bttnWijzigen;
        private System.Windows.Forms.ComboBox cbBoxFunctie;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox wachtwoordBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox achternaamBox;
        private System.Windows.Forms.TextBox voornaamBox;
        private System.Windows.Forms.Label label1;
    }
}