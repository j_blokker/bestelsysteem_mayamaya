﻿namespace bestelsysteem_mayamaya
{
    partial class VerwijderWerknemerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.bttnAnnuleren = new System.Windows.Forms.Button();
            this.verwijderWerknemerKnop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(458, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Weet u zeker dat u deze werknemer wilt verwijderen?";
            // 
            // bttnAnnuleren
            // 
            this.bttnAnnuleren.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.bttnAnnuleren.Location = new System.Drawing.Point(16, 58);
            this.bttnAnnuleren.Name = "bttnAnnuleren";
            this.bttnAnnuleren.Size = new System.Drawing.Size(131, 37);
            this.bttnAnnuleren.TabIndex = 6;
            this.bttnAnnuleren.Text = "Nee";
            this.bttnAnnuleren.UseVisualStyleBackColor = true;
            this.bttnAnnuleren.Click += new System.EventHandler(this.bttnAnnuleren_Click);
            // 
            // verwijderWerknemerKnop
            // 
            this.verwijderWerknemerKnop.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.verwijderWerknemerKnop.Location = new System.Drawing.Point(331, 58);
            this.verwijderWerknemerKnop.Name = "verwijderWerknemerKnop";
            this.verwijderWerknemerKnop.Size = new System.Drawing.Size(131, 37);
            this.verwijderWerknemerKnop.TabIndex = 7;
            this.verwijderWerknemerKnop.Text = "Ja";
            this.verwijderWerknemerKnop.UseVisualStyleBackColor = true;
            this.verwijderWerknemerKnop.Click += new System.EventHandler(this.verwijderWerknemerKnop_Click);
            // 
            // VerwijderWerknemerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 107);
            this.Controls.Add(this.bttnAnnuleren);
            this.Controls.Add(this.verwijderWerknemerKnop);
            this.Controls.Add(this.label1);
            this.Name = "VerwijderWerknemerForm";
            this.Text = "VerwijderWerknemerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bttnAnnuleren;
        private System.Windows.Forms.Button verwijderWerknemerKnop;
    }
}