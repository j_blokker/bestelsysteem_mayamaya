﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
    public partial class NieuwMedewerkerForm : Form
    {
        BestelSysteem bestelSysteem;
        public NieuwMedewerkerForm(BestelSysteem bestelSysteem)
        {
            InitializeComponent();
            this.bestelSysteem = bestelSysteem;

            foreach(Functie func in Enum.GetValues(typeof(Functie))){
                cbBoxFunctie.Items.Add(func);
            }
        }

        
        private void bttnAnnuleren_Click(object sender, EventArgs e)
        {
            this.Close();
            this.DialogResult = DialogResult.Cancel; 

        }

        //aanmaken nieuwe werknemer met btnAanmaken event
        private void btnAanmaken_Click(object sender, EventArgs e)
        {
            string Voornaam = voornaamBox.Text;
            string Achternaam = achternaamBox.Text;
            string Pincode = wachtwoordBox.Text;
            Functie functie = (Functie)cbBoxFunctie.SelectedItem;
            Werknemer werknemer = new Werknemer(0, Voornaam, Achternaam, Pincode, functie);
            bool result = bestelSysteem.NieuwWerknemer(werknemer);
            if (!result)
            {
                MessageBox.Show("Nieuwe werknemer aanmaken is niet gelukt, probeer het nogmaals");
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                this.Close();

            }
            

        }

    }
}
