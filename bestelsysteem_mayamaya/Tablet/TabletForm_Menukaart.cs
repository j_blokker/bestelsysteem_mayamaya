﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;


namespace bestelsysteem_mayamaya
{
    public partial class TabletForm_Menukaart : Form
    {
        //lists voor ieder menutype
        List<MenuItem> drankItems = new List<MenuItem>();
        List<MenuItem> lunchItems = new List<MenuItem>();
        List<MenuItem> dinerItems = new List<MenuItem>();

		Tafel tafel;

        BestelSysteem bestelSysteem;


		bool bestellingOpnemen;

        //schijden van menukaarten, tonen welke tafel is geselecteerd
		public TabletForm_Menukaart(Tafel tafel, BestelSysteem bestelSysteem, List<BestelItem> bestelItems, bool bestellingOpnemen)
        {
            InitializeComponent();
			this.tafel = tafel;
			this.bestellingOpnemen = bestellingOpnemen;

			lbl_tafelNr.Text = lbl_tafelNr.Text + " " + tafel.Nummer.ToString();
            this.bestelSysteem = bestelSysteem;

            //menukaart scheiding
            for (int i = 0; i < bestelSysteem.MenuItems.Count(); i++)
			{
                if (bestelSysteem.MenuItems[i].MenukaartId == 1) {
                    //voeg gevonden items toe aan list
                    drankItems.Add(bestelSysteem.MenuItems[i]);
                }

                else if (bestelSysteem.MenuItems[i].MenukaartId == 2)
                {

                    lunchItems.Add(bestelSysteem.MenuItems[i]);
                }

                else if (bestelSysteem.MenuItems[i].MenukaartId == 3)
                {

                    dinerItems.Add(bestelSysteem.MenuItems[i]);
                }
			}

            //vullen van kaarten
            ToonMenu(panel1, drankItems, bestelItems);
            ToonMenu(panel2, lunchItems, bestelItems);
            ToonMenu(panel3, dinerItems, bestelItems);

        }
        //opmaak voor het dynamisch menu
        int size = 31;
        int margin = 50;

        public void ToonMenu(Panel panel, List<MenuItem> items, List<BestelItem> bestelItems)
        {
            int index = 0;
            
            //categotieen scheiding
            for (int i = 0; i < bestelSysteem.Categorieen.Count; i++)
            {
                bool toonCategorie = true;
                //loop door de items heen
                for (int j = 0; j < items.Count; j++)
                {
                    //wanneer items bij categorie horen
                    if (items[j].CategorieId == bestelSysteem.Categorieen[i].Id)
                    {
                        //na het laatste item van een categorie toon volgende categorie 
                        if (toonCategorie)
                        {
                            Label lbl = new Label();
                            lbl.Top = index * margin;
                            lbl.Left = 15;
                            lbl.Name = "lblCategorie" + i.ToString();
                            lbl.Text = bestelSysteem.Categorieen[i].Naam;
                            lbl.Font = new Font(lbl_tafelNr.Font, FontStyle.Bold | FontStyle.Underline);
                            lbl.AutoSize = true;
                            panel.Controls.Add(lbl);

                            index++;
                            toonCategorie = false;
                        }
                        int aantal = 0;
                        foreach (BestelItem item in bestelItems)
                        {
                            if(items[j].Id == item.MenuItemId && item.Aantal != 0){
                                aantal = item.Aantal;
                            }
                        }
                        //method voor het menu tonen/aanmaken
                        dynamicControlls(index, items[j], panel, aantal);
                        index++;
                    }
                    //voeg lege ruimte toe na laatste menu item
                    if (bestelSysteem.Categorieen.Count - 1 == i && items.Count - 1 == j)
                    {
                        Label lbl = new Label();
                        lbl.Top = index * margin;
                        lbl.Left = 15;
                        lbl.Name = "lblEnd" + i.ToString();
                        panel.Controls.Add(lbl);
                    }
                }
            }
        }


        //Brengt gebruiker terug naar tafeloverzicht
        private void btnHome_Click(object sender, EventArgs e)
        {
            TabletForm_Tafeloverzicht tabletafeloverzicht = new TabletForm_Tafeloverzicht(bestelSysteem);
            tabletafeloverzicht.Show();

            this.Close();
        }

        //menuitems en knoppen worden aangemaakt
        private void dynamicControlls(int index, MenuItem item, Panel panel, int aantal)
        {
                Label lbl = new Label();
                lbl.Top = index * margin + 3;
                lbl.Left = 15;
                lbl.Name = "lblItem" + item.Id.ToString();
                lbl.Text = item.Naam;
                lbl.AutoSize = true;
                panel.Controls.Add(lbl);

                Label lblprijs = new Label();
                lblprijs.Top = index * margin + 3;
                lblprijs.Left = 200;
                lblprijs.TextAlign = ContentAlignment.TopRight;
                lblprijs.Name = "lblprijs" + item.Id.ToString();
                lblprijs.Text = item.Prijs.ToString("c", new CultureInfo("nl-NL"));
                lblprijs.MinimumSize = new Size(50,50);
                lblprijs.AutoSize = false;
                panel.Controls.Add(lblprijs);

                TextBox txt = new TextBox();
                txt.Top = index * margin + 1;
                txt.Left = 335;
                txt.Name = item.Id.ToString();
                txt.TextAlign = HorizontalAlignment.Center;
                txt.Height = size;
                txt.Width = size;
                txt.Text = aantal.ToString();
                panel.Controls.Add(txt);

                Button btnMin = new Button();
                btnMin.Top = index * margin;
                btnMin.Left = 300;
                btnMin.Name = "btnMinItem" + item.Id.ToString();
                btnMin.Height = size;
                btnMin.Width = size;
                btnMin.Text = "-";
                btnMin.Click += delegate
                {
                    int value = Int32.Parse(txt.Text);
                    if (value > 0)
                    {
                        value--;
                    }
                    
					//else if (value == 0){
                    //    btnMin.Enabled = false;
                    //}
                    txt.Text = value.ToString();
                };
                panel.Controls.Add(btnMin);

                

                Button btnPlus = new Button();
                btnPlus.Top = index * margin;
                btnPlus.Left = 370;
                btnPlus.Name = "btnPlusItem" + item.Id.ToString();
                btnPlus.Height = size;
                btnPlus.Width = size;
                btnPlus.Text = "+";
                btnPlus.Click += delegate
                {
                    int value = Int32.Parse(txt.Text);
                    value++;
                    txt.Text = value.ToString();
                };
                panel.Controls.Add(btnPlus);
                
            //}
        }

         //Brengt gebruiker naar bestelling overzicht
        private void btnNaarOverzicht_Click(object sender, EventArgs e)
        {
            List<BestelItem> bestelItems = new List<BestelItem>();
            foreach (TabPage page in tabcMenuKaart.TabPages)
            {
                //loop door alle items in de tabpagina
                foreach (Control control in page.Controls)
                {
                    if (control is Panel)
                    {
                        Panel panel = (Panel)control;
                        //loop door alle textboxen heen in dit panel
                        foreach (Control txt in panel.Controls.Cast<Control>().OrderBy(c => c.TabIndex))
                        {
                            //wanneer er iets anders dan 0 in de textbox staat voeg menuitem toe aan bestellijst
                            if (txt is TextBox && int.Parse(txt.Text) > 0)
                            {
                                int id = int.Parse(txt.Name);
                                int aantal = int.Parse(txt.Text);
                                BestelItem item = new BestelItem(0, id, aantal, 0, new DateTime());
                                
                                bestelItems.Add(item);
                            }

                        }
                    }
                }
            }

			TabletForm_Overzicht tabletOverzicht = new TabletForm_Overzicht(tafel, bestelSysteem, bestelItems, bestellingOpnemen);   
            
            tabletOverzicht.Show();

            this.Close();
        }

    }
}
