﻿namespace bestelsysteem_mayamaya
{
    partial class TabletForm_Keuze
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabletForm_Keuze));
            this.btnOpnemenNieuw = new System.Windows.Forms.Button();
            this.btnWijzigen = new System.Windows.Forms.Button();
            this.lbl_TafelNr = new System.Windows.Forms.Label();
            this.btnHome = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOpnemenNieuw
            // 
            this.btnOpnemenNieuw.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnOpnemenNieuw.Location = new System.Drawing.Point(97, 167);
            this.btnOpnemenNieuw.Name = "btnOpnemenNieuw";
            this.btnOpnemenNieuw.Size = new System.Drawing.Size(274, 128);
            this.btnOpnemenNieuw.TabIndex = 0;
            this.btnOpnemenNieuw.Text = "Opnemen";
            this.btnOpnemenNieuw.UseVisualStyleBackColor = true;
            this.btnOpnemenNieuw.Click += new System.EventHandler(this.btnOpnemenNieuw_Click);
            // 
            // btnWijzigen
            // 
            this.btnWijzigen.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnWijzigen.Location = new System.Drawing.Point(97, 399);
            this.btnWijzigen.Name = "btnWijzigen";
            this.btnWijzigen.Size = new System.Drawing.Size(274, 128);
            this.btnWijzigen.TabIndex = 1;
            this.btnWijzigen.Text = "Wijzigen";
            this.btnWijzigen.UseVisualStyleBackColor = true;
            this.btnWijzigen.Click += new System.EventHandler(this.btnWijzigen_Click);
            // 
            // lbl_TafelNr
            // 
            this.lbl_TafelNr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_TafelNr.AutoSize = true;
            this.lbl_TafelNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_TafelNr.Location = new System.Drawing.Point(203, 21);
            this.lbl_TafelNr.Name = "lbl_TafelNr";
            this.lbl_TafelNr.Size = new System.Drawing.Size(51, 24);
            this.lbl_TafelNr.TabIndex = 5;
            this.lbl_TafelNr.Text = "Tafel";
            // 
            // btnHome
            // 
            this.btnHome.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHome.BackgroundImage")));
            this.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHome.Location = new System.Drawing.Point(415, 12);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(49, 42);
            this.btnHome.TabIndex = 20;
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // TabletForm_Keuze
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 730);
            this.ControlBox = false;
            this.Controls.Add(this.btnHome);
            this.Controls.Add(this.lbl_TafelNr);
            this.Controls.Add(this.btnWijzigen);
            this.Controls.Add(this.btnOpnemenNieuw);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TabletForm_Keuze";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tafel opties";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpnemenNieuw;
        private System.Windows.Forms.Button btnWijzigen;
        private System.Windows.Forms.Label lbl_TafelNr;
        private System.Windows.Forms.Button btnHome;
    }
}