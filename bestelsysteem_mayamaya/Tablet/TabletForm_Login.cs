﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
    public partial class TabletForm_Login : Form
    {
		string pincode;
		BestelSysteem bestelSysteem;
		public TabletForm_Login(BestelSysteem bestelSysteem)
        {
            InitializeComponent();
			this.bestelSysteem = bestelSysteem;
			pincode = "";
        }

		private void btnNumpad_Click(object sender, EventArgs e) {
            //haal de ingedrukte knop op
			Button btnNumpad = (Button)sender;

            //limiteer het aantal getallen die in te voeren zijn
			if(pincode.Length == 4){
				return;
			}
            //vul de pincode met het getal wat in de geklikte button zit
			pincode += btnNumpad.Text;

            //toon voor iedere druk een *
			txtboxPincode.Text += "* ";

		}

		private void btnNumpadUndo_Click(object sender, EventArgs e) {
			if (pincode.Length > 0) {
                //haal 1 getal van de pincode af
				pincode = pincode.Substring(0, pincode.Length - 1);
			}
			if (txtboxPincode.Text.Length > 0) {
                //haal 2 symbolen weg (* ) uit de textbox
				txtboxPincode.Text = txtboxPincode.Text.Substring(0, txtboxPincode.Text.Length - 2);
			}
		}

		private void btnNumpadClear_Click(object sender, EventArgs e) {
            //leeg de pincode en de txtbox
			pincode = "";
			txtboxPincode.Text = "";
		}

		private void bttnLogin_Click(object sender, EventArgs e) {
            //check of de pin matcht met iemand met benodigde functie
			bestelSysteem.Login(pincode, Functie.Bediening);
            //wanneer pin matcht met werknemer
			if (bestelSysteem.IngelogdeWerknemer != null) {
                TabletForm_Tafeloverzicht tabletafeloverzicht = new TabletForm_Tafeloverzicht(bestelSysteem);
				tabletafeloverzicht.Show();

				this.Close();
			}
			else {
                //wanneer pin niet gevonden is of niet matcht met functie
				MessageBox.Show("Vul een geldig pincode in.");
				txtboxPincode.Text = "";
				pincode = "";
			}
		}
    }
}
