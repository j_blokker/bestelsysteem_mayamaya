﻿namespace bestelsysteem_mayamaya
{
    partial class TabletForm_Menukaart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabletForm_Menukaart));
            this.tabcMenuKaart = new System.Windows.Forms.TabControl();
            this.tabDranken = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabLunch = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.tabDiner = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnNaarOverzicht = new System.Windows.Forms.Button();
            this.lbl_tafelNr = new System.Windows.Forms.Label();
            this.btnHome = new System.Windows.Forms.Button();
            this.tabcMenuKaart.SuspendLayout();
            this.tabDranken.SuspendLayout();
            this.tabLunch.SuspendLayout();
            this.tabDiner.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabcMenuKaart
            // 
            this.tabcMenuKaart.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabcMenuKaart.Controls.Add(this.tabDranken);
            this.tabcMenuKaart.Controls.Add(this.tabLunch);
            this.tabcMenuKaart.Controls.Add(this.tabDiner);
            this.tabcMenuKaart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.tabcMenuKaart.Location = new System.Drawing.Point(2, 47);
            this.tabcMenuKaart.Multiline = true;
            this.tabcMenuKaart.Name = "tabcMenuKaart";
            this.tabcMenuKaart.Padding = new System.Drawing.Point(75, 6);
            this.tabcMenuKaart.SelectedIndex = 0;
            this.tabcMenuKaart.Size = new System.Drawing.Size(474, 631);
            this.tabcMenuKaart.TabIndex = 0;
            // 
            // tabDranken
            // 
            this.tabDranken.Controls.Add(this.panel1);
            this.tabDranken.Location = new System.Drawing.Point(38, 4);
            this.tabDranken.Name = "tabDranken";
            this.tabDranken.Padding = new System.Windows.Forms.Padding(3);
            this.tabDranken.Size = new System.Drawing.Size(432, 623);
            this.tabDranken.TabIndex = 0;
            this.tabDranken.Text = "Dranken";
            this.tabDranken.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Location = new System.Drawing.Point(0, -4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(436, 648);
            this.panel1.TabIndex = 0;
            // 
            // tabLunch
            // 
            this.tabLunch.Controls.Add(this.panel2);
            this.tabLunch.Controls.Add(this.listView1);
            this.tabLunch.Location = new System.Drawing.Point(38, 4);
            this.tabLunch.Name = "tabLunch";
            this.tabLunch.Padding = new System.Windows.Forms.Padding(3);
            this.tabLunch.Size = new System.Drawing.Size(432, 628);
            this.tabLunch.TabIndex = 1;
            this.tabLunch.Text = "Lunch";
            this.tabLunch.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Location = new System.Drawing.Point(0, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(431, 643);
            this.panel2.TabIndex = 1;
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(436, 673);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // tabDiner
            // 
            this.tabDiner.Controls.Add(this.panel3);
            this.tabDiner.Location = new System.Drawing.Point(38, 4);
            this.tabDiner.Name = "tabDiner";
            this.tabDiner.Size = new System.Drawing.Size(432, 628);
            this.tabDiner.TabIndex = 2;
            this.tabDiner.Text = "Diner";
            this.tabDiner.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.Location = new System.Drawing.Point(1, -1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(431, 643);
            this.panel3.TabIndex = 1;
            // 
            // btnNaarOverzicht
            // 
            this.btnNaarOverzicht.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNaarOverzicht.Location = new System.Drawing.Point(79, 684);
            this.btnNaarOverzicht.Name = "btnNaarOverzicht";
            this.btnNaarOverzicht.Size = new System.Drawing.Size(323, 37);
            this.btnNaarOverzicht.TabIndex = 1;
            this.btnNaarOverzicht.Text = "Naar overzicht";
            this.btnNaarOverzicht.UseVisualStyleBackColor = true;
            this.btnNaarOverzicht.Click += new System.EventHandler(this.btnNaarOverzicht_Click);
            // 
            // lbl_tafelNr
            // 
            this.lbl_tafelNr.AutoSize = true;
            this.lbl_tafelNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_tafelNr.Location = new System.Drawing.Point(199, 3);
            this.lbl_tafelNr.Name = "lbl_tafelNr";
            this.lbl_tafelNr.Size = new System.Drawing.Size(51, 24);
            this.lbl_tafelNr.TabIndex = 2;
            this.lbl_tafelNr.Text = "Tafel";
            // 
            // btnHome
            // 
            this.btnHome.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHome.BackgroundImage")));
            this.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHome.Location = new System.Drawing.Point(421, 1);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(55, 49);
            this.btnHome.TabIndex = 21;
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // TabletForm_Menukaart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 730);
            this.ControlBox = false;
            this.Controls.Add(this.btnHome);
            this.Controls.Add(this.lbl_tafelNr);
            this.Controls.Add(this.btnNaarOverzicht);
            this.Controls.Add(this.tabcMenuKaart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TabletForm_Menukaart";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menukaart";
            this.tabcMenuKaart.ResumeLayout(false);
            this.tabDranken.ResumeLayout(false);
            this.tabLunch.ResumeLayout(false);
            this.tabDiner.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabcMenuKaart;
        private System.Windows.Forms.TabPage tabLunch;
        private System.Windows.Forms.TabPage tabDiner;
        private System.Windows.Forms.Button btnNaarOverzicht;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label lbl_tafelNr;
        private System.Windows.Forms.TabPage tabDranken;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnHome;
    }
}