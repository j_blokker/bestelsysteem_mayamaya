﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Data.SqlClient;

namespace bestelsysteem_mayamaya
{
    public partial class TabletForm_Overzicht : Form
    {
        BestelSysteem bestelSysteem;
        List<BestelItem> bestelItems;
		bool bestellingOpnemen;

        Tafel tafel;
        public TabletForm_Overzicht(Tafel tafel, BestelSysteem bestelSysteem, List<BestelItem> bestelItems, bool bestellingOpnemen)
        {
            InitializeComponent();
			this.tafel = tafel;
            //toon tafel nummer op form
			lbl_TafelNr.Text = lbl_TafelNr.Text + " " + tafel.Nummer.ToString();

            this.bestelSysteem = bestelSysteem;
            this.bestelItems = bestelItems;
			this.bestellingOpnemen = bestellingOpnemen;

            foreach (BestelItem bestelItem in bestelItems)
            {
                //match ieder bestelitem
				MenuItem mItem = bestelSysteem.ZoekMenuItemOpMenuItemId(bestelItem.MenuItemId);
                //bereken prijs van bij elkaar opgetelde items
				double subPrijs = mItem.Prijs * Convert.ToDouble(bestelItem.Aantal);

                //voeg items toe aan list view
                ListViewItem colomm = new ListViewItem();
				colomm.SubItems.Add(bestelItem.Aantal.ToString() + "x");
				colomm.SubItems.Add(mItem.Naam);
                //toon de prijs in euros
                colomm.SubItems.Add(subPrijs.ToString("c", new CultureInfo("nl-NL")));
                listv_overzicht.Items.Add(colomm);
            }
        }
        private void btnHome_Click(object sender, EventArgs e)
        {
            TabletForm_Tafeloverzicht tabletafeloverzicht = new TabletForm_Tafeloverzicht(bestelSysteem);
            tabletafeloverzicht.Show();

            this.Close();
        }
        private void btnPlaatsBestelling_Click(object sender, EventArgs e)
        {
			int bestellingId = 0;
			DateTime opgenomenTijd = DateTime.Now;
			TabletForm_Tafeloverzicht tabletTafelOverzicht = new TabletForm_Tafeloverzicht(bestelSysteem);
			
			//Bestellijst mag niet leeg zijn
			if (bestelItems == null || bestelItems.Count == 0) {
				MessageBox.Show("Fout bij het plaatsen van de bestelling.\n Er is niets besteld!", "Foutje", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
            
			//Haal eventuele lopende bestelling op
            foreach (Bestelling bestelling in bestelSysteem.Bestellingen)
            {
				if (bestelling.TafelId == tafel.Id && !bestelling.Afgerond)
                {
                    bestellingId = bestelling.Id;
					break;
                }
            }

			if (bestellingOpnemen) {
				if (bestellingId == 0) {
					//Nieuwe bestelling toevoegen.
					bestellingId = bestelSysteem.AanmakenNieuweBestelling(tafel.Id);

                    //wanneer geen bestelling id word toegevoegd
					if (bestellingId == 0) {
						MessageBox.Show("Kon geen verbinding maken met de database, probeer het later nogmaals.");
						return;
					}
				}

				//Invoeren bestelde item
				foreach (BestelItem item in bestelItems) {
					bool gevonden = false;
					foreach (BestelItem bItem in bestelSysteem.BestelItems) {
						if (bestellingId == bItem.BestellingId && item.MenuItemId == bItem.MenuItemId) {
							item.Aantal += bItem.Aantal;
							gevonden = true;
							break;
						}
					}
					bool result;
                    //check of het item
					if (gevonden) {
						result = bestelSysteem.WijzigenBesteldeItem(bestellingId, item, opgenomenTijd);
					}
					else {
						result = bestelSysteem.InvoerenBesteldeItem(bestellingId, item, opgenomenTijd);
					}

					if (!result) {
						MessageBox.Show("Kon geen verbinding maken met de database, probeer het later nogmaals.");
						return;
					}
				}
			}
			else {
				if (bestellingId == 0) {
					MessageBox.Show("Geen lopende bestelling gevonden");
					return;
				}

				//Wijzigen van bestaande bestel item
				foreach (BestelItem item in bestelItems) {
					foreach (BestelItem bItem in bestelSysteem.BestelItems) {
						if (bestellingId == bItem.BestellingId && item.MenuItemId == bItem.MenuItemId) {
							if (item.Aantal != bItem.Aantal && item.Aantal > 0) {
								bool result = bestelSysteem.WijzigenBesteldeItem(bestellingId, item, opgenomenTijd);

								if (!result) {
									MessageBox.Show("Kon geen verbinding maken met de database, probeer het later nogmaals.");
									return;
								}
							}
							break;
						}
					}
				}

				//eventuele items die niet in bestellijst staan verwijderen uit DB
				if (bestellingId > 0) {
					for (int i = bestelSysteem.BestelItems.Count - 1; i > 0; i--) {
						bool remove = false;

						if (bestelSysteem.BestelItems[i].BestellingId == bestellingId) {
							remove = true;
						}
                        //loop door bestelitems heen
						foreach (BestelItem item in bestelItems) {
							if (bestellingId == bestelSysteem.BestelItems[i].BestellingId && bestelSysteem.BestelItems[i].MenuItemId == item.MenuItemId) {
								remove = false;
								break;
							}
						}

						if (remove) {
                            //verwijder uit DB
							bestelSysteem.VerwijderenBesteldeItem(bestellingId, bestelSysteem.BestelItems[i]);
						}
					}
				}
			}

			tabletTafelOverzicht.Show();
            this.Close();
        }

        private void btnTerug_Click(object sender, EventArgs e)
        {
			TabletForm_Menukaart tabletMenu = new TabletForm_Menukaart(tafel, bestelSysteem, bestelItems, bestellingOpnemen);

            tabletMenu.Show();

            this.Close();
        }
    }
}
