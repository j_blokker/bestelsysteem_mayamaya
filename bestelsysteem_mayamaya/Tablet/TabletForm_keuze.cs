﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
    public partial class TabletForm_Keuze : Form
    {
        Tafel tafel;
        BestelSysteem bestelSysteem;

        public TabletForm_Keuze(Tafel tafel, BestelSysteem bestelSysteem)
        {
            InitializeComponent();
			this.tafel = tafel;

            //toon tafel nummer op scherm
			lbl_TafelNr.Text = lbl_TafelNr.Text + " " + tafel.Nummer.ToString();
            this.bestelSysteem = bestelSysteem;
        }

        private void btnOpnemenNieuw_Click(object sender, EventArgs e)
        {
			TabletForm_Menukaart tabletMenu = new TabletForm_Menukaart(tafel, bestelSysteem, new List<BestelItem>(), true);
            tabletMenu.Show();
            this.Close();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            TabletForm_Tafeloverzicht tabletafeloverzicht = new TabletForm_Tafeloverzicht(bestelSysteem);
            tabletafeloverzicht.Show();
            this.Close();
        }

        private void btnWijzigen_Click(object sender, EventArgs e)
        {
			Bestelling bestelling = null;
			foreach (Bestelling item in bestelSysteem.Bestellingen) {
                //zoek of er een bestelling bestaat voor deze tafel
				if (item.TafelId == tafel.Id && !item.Afgerond) {
					//Er loopt nog een bestelling!
					bestelling = item;
				}
			}
            //als er geen bestelling is gevonden
			if(bestelling == null){
				MessageBox.Show("Er is geen lopende bestelling voor deze tafel gevonden!");
				return;
			}

			//Bij gevonde bestelling, ophalen bestelItems
			List<BestelItem> besteldeItems = new List<BestelItem>();
			foreach(BestelItem item in bestelSysteem.BestelItems){
				if (item.BestellingId == bestelling.Id) {
					besteldeItems.Add(item);
				}
			}
			TabletForm_Menukaart tabletMenu = new TabletForm_Menukaart(tafel, bestelSysteem, besteldeItems, false);
            tabletMenu.Show();
            this.Close();
        }
    }
}

