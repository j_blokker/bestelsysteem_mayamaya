﻿namespace bestelsysteem_mayamaya
{
    partial class TabletForm_Overzicht
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TabletForm_Overzicht));
            this.listv_overzicht = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lv_aantal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lv_naam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lv_prijs = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lbl_TafelNr = new System.Windows.Forms.Label();
            this.btnNaarOverzicht = new System.Windows.Forms.Button();
            this.btnPlaatsBestelling = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listv_overzicht
            // 
            this.listv_overzicht.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.lv_aantal,
            this.lv_naam,
            this.lv_prijs});
            this.listv_overzicht.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.listv_overzicht.Location = new System.Drawing.Point(1, 47);
            this.listv_overzicht.Name = "listv_overzicht";
            this.listv_overzicht.Size = new System.Drawing.Size(459, 587);
            this.listv_overzicht.TabIndex = 0;
            this.listv_overzicht.UseCompatibleStateImageBehavior = false;
            this.listv_overzicht.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.DisplayIndex = 3;
            this.columnHeader1.Width = 0;
            // 
            // lv_aantal
            // 
            this.lv_aantal.DisplayIndex = 0;
            this.lv_aantal.Text = "Aantal";
            this.lv_aantal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lv_aantal.Width = 75;
            // 
            // lv_naam
            // 
            this.lv_naam.DisplayIndex = 1;
            this.lv_naam.Text = "Naam";
            this.lv_naam.Width = 286;
            // 
            // lv_prijs
            // 
            this.lv_prijs.DisplayIndex = 2;
            this.lv_prijs.Text = "Prijs";
            this.lv_prijs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.lv_prijs.Width = 85;
            // 
            // lbl_TafelNr
            // 
            this.lbl_TafelNr.AutoSize = true;
            this.lbl_TafelNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbl_TafelNr.Location = new System.Drawing.Point(198, 6);
            this.lbl_TafelNr.Name = "lbl_TafelNr";
            this.lbl_TafelNr.Size = new System.Drawing.Size(51, 24);
            this.lbl_TafelNr.TabIndex = 1;
            this.lbl_TafelNr.Text = "Tafel";
            // 
            // btnNaarOverzicht
            // 
            this.btnNaarOverzicht.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNaarOverzicht.Location = new System.Drawing.Point(28, 644);
            this.btnNaarOverzicht.Name = "btnNaarOverzicht";
            this.btnNaarOverzicht.Size = new System.Drawing.Size(179, 37);
            this.btnNaarOverzicht.TabIndex = 4;
            this.btnNaarOverzicht.Text = "Terug";
            this.btnNaarOverzicht.UseVisualStyleBackColor = true;
            this.btnNaarOverzicht.Click += new System.EventHandler(this.btnTerug_Click);
            // 
            // btnPlaatsBestelling
            // 
            this.btnPlaatsBestelling.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnPlaatsBestelling.Location = new System.Drawing.Point(236, 644);
            this.btnPlaatsBestelling.Name = "btnPlaatsBestelling";
            this.btnPlaatsBestelling.Size = new System.Drawing.Size(179, 37);
            this.btnPlaatsBestelling.TabIndex = 4;
            this.btnPlaatsBestelling.Text = "Plaats bestelling";
            this.btnPlaatsBestelling.UseVisualStyleBackColor = true;
            this.btnPlaatsBestelling.Click += new System.EventHandler(this.btnPlaatsBestelling_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHome.BackgroundImage")));
            this.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHome.Location = new System.Drawing.Point(406, -1);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(54, 49);
            this.btnHome.TabIndex = 20;
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // TabletForm_Overzicht
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 691);
            this.ControlBox = false;
            this.Controls.Add(this.btnHome);
            this.Controls.Add(this.btnPlaatsBestelling);
            this.Controls.Add(this.btnNaarOverzicht);
            this.Controls.Add(this.lbl_TafelNr);
            this.Controls.Add(this.listv_overzicht);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TabletForm_Overzicht";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Overzicht";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listv_overzicht;
        private System.Windows.Forms.Label lbl_TafelNr;
        private System.Windows.Forms.Button btnNaarOverzicht;
        private System.Windows.Forms.Button btnPlaatsBestelling;
        private System.Windows.Forms.ColumnHeader lv_aantal;
        private System.Windows.Forms.ColumnHeader lv_naam;
        private System.Windows.Forms.ColumnHeader lv_prijs;
		private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Button btnHome;
    }
}