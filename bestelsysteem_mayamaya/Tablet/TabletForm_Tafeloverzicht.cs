﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{

    public partial class TabletForm_Tafeloverzicht : Form
    {
        BestelSysteem bestelSysteem;
        int tafelNr = 0;

        public TabletForm_Tafeloverzicht(BestelSysteem bestelSysteem)
        {
            InitializeComponent();
            this.bestelSysteem = bestelSysteem;
        }

        private void TabletTafelOverzicht_Shown(object sender, EventArgs e)
        {
            //haal de datum van vandaag op
            DateTime dateTime = DateTime.Now;
            DateTime date = dateTime.Date;

            //vul de naam van de ingelogde medewerker in
            lblIngelogdeMedewerker.Text = bestelSysteem.IngelogdeWerknemer.Voornaam + " " + bestelSysteem.IngelogdeWerknemer.Achternaam;

            //loop door alle buttons in panel
			foreach (Control button in this.pnlTafelBtn.Controls) {
				if (button is Button) {
					//haal tafelnummer op uit de knoppen
                    bool result = int.TryParse(((Button)button).Text.Replace("Tafel ", ""), out tafelNr);
                    if (!result) {
                        MessageBox.Show("Er is iets fout gegaan met het ophalen van het tafel nummer \n Probeer het nog maals.");
                    }

                    //koppel het tafelnummer met het nummer uit de database(lijst)
					int tafelId = bestelSysteem.ZoekTafelOpTafelNummer(tafelNr).Id;
					int bestellingId = 0;

					foreach (Bestelling bestelling in bestelSysteem.Bestellingen) {
                        //als er een bestaande bestelling op deze tafel is, haal het tafelid op
						if (tafelId == bestelling.TafelId && bestelling.Afgerond == false) {
							bestellingId = bestelling.Id;
							break;
						}
					}
					foreach (BestelItem bestelItem in bestelSysteem.BestelItems) {
                        //check of de bestelling voor vandaag staat
						if (bestelItem.BestellingId == bestellingId && date == bestelItem.Opgenomen.Date) {
							((Button)button).BackColor = Color.Red;
							break;
						}
					}
				}
			}
        }

        private void btnTafel_Click(object sender, EventArgs e)
        {
            int tafelNr;
            //haal de informatie uit de gedrukte knop
            Button btnTafel = (Button)sender;
            bool result = int.TryParse(btnTafel.Text.Replace("Tafel ", ""), out tafelNr);
            if (!result)
            {
                MessageBox.Show("Er is iets fout gegaan met het ophalen van het tafel nummer \n Probeer het nog maals.");
            }

			//Ophalen van tafel object
			Tafel tafel = bestelSysteem.ZoekTafelOpTafelNummer(tafelNr);

            //maak form aan
			TabletForm_Keuze tabletKeuze = new TabletForm_Keuze(tafel, bestelSysteem);
           
            //open form
            tabletKeuze.Show();
            this.Close();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            //ga terug naar login scherm
			TabletForm_Login tabletLogin = new TabletForm_Login(bestelSysteem);

            //log gebruiker uit door gebruiker op null te zetten
            bestelSysteem.Logout();
            tabletLogin.Show();

            this.Close();
            
        }
    }
}
