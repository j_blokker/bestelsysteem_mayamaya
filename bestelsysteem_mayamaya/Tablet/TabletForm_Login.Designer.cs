﻿namespace bestelsysteem_mayamaya
{
    partial class TabletForm_Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtboxPincode = new System.Windows.Forms.TextBox();
            this.lblPincode = new System.Windows.Forms.Label();
            this.bttnLogin = new System.Windows.Forms.Button();
            this.btnNumpad3 = new System.Windows.Forms.Button();
            this.btnNumpad2 = new System.Windows.Forms.Button();
            this.btnNumpad1 = new System.Windows.Forms.Button();
            this.btnNumpad4 = new System.Windows.Forms.Button();
            this.btnNumpad5 = new System.Windows.Forms.Button();
            this.btnNumpad6 = new System.Windows.Forms.Button();
            this.btnNumpad7 = new System.Windows.Forms.Button();
            this.btnNumpad8 = new System.Windows.Forms.Button();
            this.btnNumpad9 = new System.Windows.Forms.Button();
            this.btnNumpad0 = new System.Windows.Forms.Button();
            this.btnNumpadUndo = new System.Windows.Forms.Button();
            this.btnNumpadClear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtboxPincode
            // 
            this.txtboxPincode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtboxPincode.Location = new System.Drawing.Point(169, 63);
            this.txtboxPincode.Name = "txtboxPincode";
            this.txtboxPincode.Size = new System.Drawing.Size(137, 29);
            this.txtboxPincode.TabIndex = 0;
            this.txtboxPincode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblPincode
            // 
            this.lblPincode.AutoSize = true;
            this.lblPincode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPincode.Location = new System.Drawing.Point(64, 67);
            this.lblPincode.Name = "lblPincode";
            this.lblPincode.Size = new System.Drawing.Size(99, 24);
            this.lblPincode.TabIndex = 1;
            this.lblPincode.Text = "PINCODE:";
            // 
            // bttnLogin
            // 
            this.bttnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.bttnLogin.Location = new System.Drawing.Point(169, 119);
            this.bttnLogin.Name = "bttnLogin";
            this.bttnLogin.Size = new System.Drawing.Size(137, 37);
            this.bttnLogin.TabIndex = 2;
            this.bttnLogin.Text = "Login";
            this.bttnLogin.UseVisualStyleBackColor = true;
            this.bttnLogin.Click += new System.EventHandler(this.bttnLogin_Click);
            // 
            // btnNumpad3
            // 
            this.btnNumpad3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpad3.Location = new System.Drawing.Point(334, 201);
            this.btnNumpad3.Name = "btnNumpad3";
            this.btnNumpad3.Size = new System.Drawing.Size(100, 100);
            this.btnNumpad3.TabIndex = 3;
            this.btnNumpad3.Text = "3";
            this.btnNumpad3.UseVisualStyleBackColor = true;
            this.btnNumpad3.Click += new System.EventHandler(this.btnNumpad_Click);
            // 
            // btnNumpad2
            // 
            this.btnNumpad2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpad2.Location = new System.Drawing.Point(189, 201);
            this.btnNumpad2.Name = "btnNumpad2";
            this.btnNumpad2.Size = new System.Drawing.Size(100, 100);
            this.btnNumpad2.TabIndex = 4;
            this.btnNumpad2.Text = "2";
            this.btnNumpad2.UseVisualStyleBackColor = true;
            this.btnNumpad2.Click += new System.EventHandler(this.btnNumpad_Click);
            // 
            // btnNumpad1
            // 
            this.btnNumpad1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpad1.Location = new System.Drawing.Point(44, 201);
            this.btnNumpad1.Name = "btnNumpad1";
            this.btnNumpad1.Size = new System.Drawing.Size(100, 100);
            this.btnNumpad1.TabIndex = 5;
            this.btnNumpad1.Text = "1";
            this.btnNumpad1.UseVisualStyleBackColor = true;
            this.btnNumpad1.Click += new System.EventHandler(this.btnNumpad_Click);
            // 
            // btnNumpad4
            // 
            this.btnNumpad4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpad4.Location = new System.Drawing.Point(44, 326);
            this.btnNumpad4.Name = "btnNumpad4";
            this.btnNumpad4.Size = new System.Drawing.Size(100, 100);
            this.btnNumpad4.TabIndex = 5;
            this.btnNumpad4.Text = "4";
            this.btnNumpad4.UseVisualStyleBackColor = true;
            this.btnNumpad4.Click += new System.EventHandler(this.btnNumpad_Click);
            // 
            // btnNumpad5
            // 
            this.btnNumpad5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpad5.Location = new System.Drawing.Point(189, 326);
            this.btnNumpad5.Name = "btnNumpad5";
            this.btnNumpad5.Size = new System.Drawing.Size(100, 100);
            this.btnNumpad5.TabIndex = 4;
            this.btnNumpad5.Text = "5";
            this.btnNumpad5.UseVisualStyleBackColor = true;
            this.btnNumpad5.Click += new System.EventHandler(this.btnNumpad_Click);
            // 
            // btnNumpad6
            // 
            this.btnNumpad6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpad6.Location = new System.Drawing.Point(334, 326);
            this.btnNumpad6.Name = "btnNumpad6";
            this.btnNumpad6.Size = new System.Drawing.Size(100, 100);
            this.btnNumpad6.TabIndex = 3;
            this.btnNumpad6.Text = "6";
            this.btnNumpad6.UseVisualStyleBackColor = true;
            this.btnNumpad6.Click += new System.EventHandler(this.btnNumpad_Click);
            // 
            // btnNumpad7
            // 
            this.btnNumpad7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpad7.Location = new System.Drawing.Point(44, 450);
            this.btnNumpad7.Name = "btnNumpad7";
            this.btnNumpad7.Size = new System.Drawing.Size(100, 100);
            this.btnNumpad7.TabIndex = 5;
            this.btnNumpad7.Text = "7";
            this.btnNumpad7.UseVisualStyleBackColor = true;
            this.btnNumpad7.Click += new System.EventHandler(this.btnNumpad_Click);
            // 
            // btnNumpad8
            // 
            this.btnNumpad8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpad8.Location = new System.Drawing.Point(189, 450);
            this.btnNumpad8.Name = "btnNumpad8";
            this.btnNumpad8.Size = new System.Drawing.Size(100, 100);
            this.btnNumpad8.TabIndex = 4;
            this.btnNumpad8.Text = "8";
            this.btnNumpad8.UseVisualStyleBackColor = true;
            this.btnNumpad8.Click += new System.EventHandler(this.btnNumpad_Click);
            // 
            // btnNumpad9
            // 
            this.btnNumpad9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpad9.Location = new System.Drawing.Point(334, 450);
            this.btnNumpad9.Name = "btnNumpad9";
            this.btnNumpad9.Size = new System.Drawing.Size(100, 100);
            this.btnNumpad9.TabIndex = 3;
            this.btnNumpad9.Text = "9";
            this.btnNumpad9.UseVisualStyleBackColor = true;
            this.btnNumpad9.Click += new System.EventHandler(this.btnNumpad_Click);
            // 
            // btnNumpad0
            // 
            this.btnNumpad0.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpad0.Location = new System.Drawing.Point(189, 574);
            this.btnNumpad0.Name = "btnNumpad0";
            this.btnNumpad0.Size = new System.Drawing.Size(100, 100);
            this.btnNumpad0.TabIndex = 4;
            this.btnNumpad0.Text = "0";
            this.btnNumpad0.UseVisualStyleBackColor = true;
            this.btnNumpad0.Click += new System.EventHandler(this.btnNumpad_Click);
            // 
            // btnNumpadUndo
            // 
            this.btnNumpadUndo.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNumpadUndo.Location = new System.Drawing.Point(44, 574);
            this.btnNumpadUndo.Name = "btnNumpadUndo";
            this.btnNumpadUndo.Size = new System.Drawing.Size(100, 100);
            this.btnNumpadUndo.TabIndex = 5;
            this.btnNumpadUndo.Text = "←";
            this.btnNumpadUndo.UseVisualStyleBackColor = true;
            this.btnNumpadUndo.Click += new System.EventHandler(this.btnNumpadUndo_Click);
            // 
            // btnNumpadClear
            // 
            this.btnNumpadClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnNumpadClear.Location = new System.Drawing.Point(334, 574);
            this.btnNumpadClear.Name = "btnNumpadClear";
            this.btnNumpadClear.Size = new System.Drawing.Size(100, 100);
            this.btnNumpadClear.TabIndex = 3;
            this.btnNumpadClear.Text = "Clear";
            this.btnNumpadClear.UseVisualStyleBackColor = true;
            this.btnNumpadClear.Click += new System.EventHandler(this.btnNumpadClear_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label1.Location = new System.Drawing.Point(212, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Login";
            // 
            // TabletForm_Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 730);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnNumpadClear);
            this.Controls.Add(this.btnNumpad9);
            this.Controls.Add(this.btnNumpad6);
            this.Controls.Add(this.btnNumpad3);
            this.Controls.Add(this.btnNumpad0);
            this.Controls.Add(this.btnNumpadUndo);
            this.Controls.Add(this.btnNumpad8);
            this.Controls.Add(this.btnNumpad7);
            this.Controls.Add(this.btnNumpad5);
            this.Controls.Add(this.btnNumpad4);
            this.Controls.Add(this.btnNumpad2);
            this.Controls.Add(this.btnNumpad1);
            this.Controls.Add(this.bttnLogin);
            this.Controls.Add(this.lblPincode);
            this.Controls.Add(this.txtboxPincode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TabletForm_Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtboxPincode;
        private System.Windows.Forms.Label lblPincode;
        private System.Windows.Forms.Button bttnLogin;
		private System.Windows.Forms.Button btnNumpad3;
		private System.Windows.Forms.Button btnNumpad2;
		private System.Windows.Forms.Button btnNumpad1;
		private System.Windows.Forms.Button btnNumpad4;
		private System.Windows.Forms.Button btnNumpad5;
		private System.Windows.Forms.Button btnNumpad6;
		private System.Windows.Forms.Button btnNumpad7;
		private System.Windows.Forms.Button btnNumpad8;
		private System.Windows.Forms.Button btnNumpad9;
		private System.Windows.Forms.Button btnNumpad0;
		private System.Windows.Forms.Button btnNumpadUndo;
		private System.Windows.Forms.Button btnNumpadClear;
        private System.Windows.Forms.Label label1;
    }
}