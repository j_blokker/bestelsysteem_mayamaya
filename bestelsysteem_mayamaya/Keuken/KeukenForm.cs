﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
	public partial class KeukenForm : Form
	{
		private BestelSysteem bestelSysteem;

		public KeukenForm(BestelSysteem bestelSysteem) {
			InitializeComponent();

			this.bestelSysteem = bestelSysteem;

			//categorie dropdown vullen
			comBoCategorieen.Items.Clear();
			comBoCategorieen.Items.Add(new Categorie(0, "Alles"));
			comBoCategorieen.SelectedIndex = 0;
			
			for (int i = 0; i < 4; i++) {
				comBoCategorieen.Items.Add(bestelSysteem.Categorieen[i]);
			}

			Categorie selectedCategorie = (Categorie)comBoCategorieen.SelectedItem;

			WeergevenBestelItemsKeuken(selectedCategorie.Id);
		}

		private void WeergevenBestelItemsKeuken(int categorieId) {
			List<BestelItem> besteldeItems = bestelSysteem.OphalenBestelItemsKeuken();
			lvBestelItemsInhoud.Items.Clear();

			//Loopen door bestelItems en weergeven in de ListView
			foreach(BestelItem item in besteldeItems){
				//Haal tafelNr op via bestellingId
				Bestelling bestelling = bestelSysteem.ZoekBestellingOpBestellingId(item.BestellingId);
				Tafel tafel = bestelSysteem.ZoekTafelOpTafelId(bestelling.TafelId);

				//Haal menuItem naam op via menuItemId
				MenuItem menuItem = bestelSysteem.ZoekMenuItemOpMenuItemId(item.MenuItemId);

				if (menuItem == null) {
					MessageBox.Show("Er is een fout opgetreden bij het samenstellen van de bestellingenlijst, probeer het later nogmaals.\nIndien deze fout zich herhaalt, neem dan contact op met uw administrator.", "Systeemfout", MessageBoxButtons.OK, MessageBoxIcon.Error);

					bestelSysteem.Logout();
					return;
				}

				//Filter op categorie
				if (categorieId > 0 && menuItem.CategorieId != categorieId) {
					continue;
				}

				//ListViewItem aanmaken en toevoegen
				ListViewItem lvItem = new ListViewItem(item.BestellingId.ToString());
				lvItem.SubItems.Add(item.MenuItemId.ToString());
				lvItem.SubItems.Add(item.Aantal.ToString() + "x");
				lvItem.SubItems.Add(menuItem.Omschrijving);
				lvItem.SubItems.Add(tafel.Nummer.ToString());
				lvItem.SubItems.Add(item.Opgenomen.ToString("HH:mm"));

				lvBestelItemsInhoud.Items.Add(lvItem);
			}
		}

		#region Events
		private void btnLogout_Click(object sender, EventArgs e) {
			bestelSysteem.Logout();
			this.Close();
		}

		private void KeukenForm_Shown(object sender, EventArgs e) {
			lblIngelogdeMedewerker.Text = bestelSysteem.IngelogdeWerknemer.Voornaam + " " + bestelSysteem.IngelogdeWerknemer.Achternaam;
		}

		private void btnGereedmelden_Click(object sender, EventArgs e) {
			List<BestelItem> besteldeItems = bestelSysteem.OphalenBestelItemsKeuken();

			//Doe niets wanneer er niets geselecteerd is
			if (lvBestelItemsInhoud.SelectedItems.Count == 0) {
				return;
			}

			//Ophalen bestellingId en tijd van opnemen
			foreach (ListViewItem item in lvBestelItemsInhoud.SelectedItems) {
				int bestellingId = Convert.ToInt32(item.SubItems[0].Text);
				int menuItemId = Convert.ToInt32(item.SubItems[1].Text);

				foreach (BestelItem bestelItem in besteldeItems) {
					//Haal items overeenkomend met bestelling id en opgenomen
					if (bestelItem.BestellingId == bestellingId && bestelItem.MenuItemId == menuItemId) {
						//Meldt gevonden items gereed in DB in list
						bestelSysteem.GereedmeldenBestellingItem(bestelItem);
					}
				}
			}

			//update interface
			Categorie selectedCategorie = (Categorie)comBoCategorieen.SelectedItem;
			WeergevenBestelItemsKeuken(selectedCategorie.Id);
		}

		private void comBoCategorieen_SelectedIndexChanged(object sender, EventArgs e) {
			Categorie selectedCategorie = (Categorie)comBoCategorieen.SelectedItem;
			WeergevenBestelItemsKeuken(selectedCategorie.Id);
		}

		#endregion
	}
}
