﻿namespace bestelsysteem_mayamaya
{
	partial class KeukenForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KeukenForm));
			this.lvBestelItemsInhoud = new System.Windows.Forms.ListView();
			this.lvBestelItemsInhoudBestellingId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestelItemsInhoudColMenuItemId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestelItemsInhoudColMenuItemAantal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestelItemsInhoudColMenuItemNaam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestelItemsInhoudColTafelNr = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.btnLogout = new System.Windows.Forms.Button();
			this.lblIngelogdeMedewerker = new System.Windows.Forms.Label();
			this.comBoCategorieen = new System.Windows.Forms.ComboBox();
			this.btnGereedmelden = new System.Windows.Forms.Button();
			this.lvBestelItemsInhoudColOpgenomen = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.SuspendLayout();
			// 
			// lvBestelItemsInhoud
			// 
			this.lvBestelItemsInhoud.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvBestelItemsInhoudBestellingId,
            this.lvBestelItemsInhoudColMenuItemId,
            this.lvBestelItemsInhoudColMenuItemAantal,
            this.lvBestelItemsInhoudColMenuItemNaam,
            this.lvBestelItemsInhoudColTafelNr,
            this.lvBestelItemsInhoudColOpgenomen});
			this.lvBestelItemsInhoud.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lvBestelItemsInhoud.FullRowSelect = true;
			this.lvBestelItemsInhoud.GridLines = true;
			this.lvBestelItemsInhoud.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvBestelItemsInhoud.Location = new System.Drawing.Point(36, 74);
			this.lvBestelItemsInhoud.Name = "lvBestelItemsInhoud";
			this.lvBestelItemsInhoud.Size = new System.Drawing.Size(890, 521);
			this.lvBestelItemsInhoud.TabIndex = 4;
			this.lvBestelItemsInhoud.TileSize = new System.Drawing.Size(212, 40);
			this.lvBestelItemsInhoud.UseCompatibleStateImageBehavior = false;
			this.lvBestelItemsInhoud.View = System.Windows.Forms.View.Details;
			// 
			// lvBestelItemsInhoudBestellingId
			// 
			this.lvBestelItemsInhoudBestellingId.DisplayIndex = 3;
			this.lvBestelItemsInhoudBestellingId.Width = 0;
			// 
			// lvBestelItemsInhoudColMenuItemId
			// 
			this.lvBestelItemsInhoudColMenuItemId.DisplayIndex = 4;
			this.lvBestelItemsInhoudColMenuItemId.Width = 0;
			// 
			// lvBestelItemsInhoudColMenuItemAantal
			// 
			this.lvBestelItemsInhoudColMenuItemAantal.DisplayIndex = 0;
			this.lvBestelItemsInhoudColMenuItemAantal.Text = "Aantal";
			this.lvBestelItemsInhoudColMenuItemAantal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.lvBestelItemsInhoudColMenuItemAantal.Width = 90;
			// 
			// lvBestelItemsInhoudColMenuItemNaam
			// 
			this.lvBestelItemsInhoudColMenuItemNaam.DisplayIndex = 1;
			this.lvBestelItemsInhoudColMenuItemNaam.Tag = "";
			this.lvBestelItemsInhoudColMenuItemNaam.Text = "Naam";
			this.lvBestelItemsInhoudColMenuItemNaam.Width = 605;
			// 
			// lvBestelItemsInhoudColTafelNr
			// 
			this.lvBestelItemsInhoudColTafelNr.DisplayIndex = 2;
			this.lvBestelItemsInhoudColTafelNr.Text = "Tafel";
			this.lvBestelItemsInhoudColTafelNr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.lvBestelItemsInhoudColTafelNr.Width = 95;
			// 
			// btnLogout
			// 
			this.btnLogout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLogout.BackgroundImage")));
			this.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnLogout.FlatAppearance.BorderSize = 0;
			this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.btnLogout.Location = new System.Drawing.Point(1181, 9);
			this.btnLogout.Margin = new System.Windows.Forms.Padding(0);
			this.btnLogout.Name = "btnLogout";
			this.btnLogout.Size = new System.Drawing.Size(45, 45);
			this.btnLogout.TabIndex = 6;
			this.btnLogout.UseVisualStyleBackColor = true;
			this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
			// 
			// lblIngelogdeMedewerker
			// 
			this.lblIngelogdeMedewerker.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblIngelogdeMedewerker.Location = new System.Drawing.Point(728, 19);
			this.lblIngelogdeMedewerker.Name = "lblIngelogdeMedewerker";
			this.lblIngelogdeMedewerker.Size = new System.Drawing.Size(450, 24);
			this.lblIngelogdeMedewerker.TabIndex = 5;
			this.lblIngelogdeMedewerker.Text = "[Medewerker]";
			this.lblIngelogdeMedewerker.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// comBoCategorieen
			// 
			this.comBoCategorieen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comBoCategorieen.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.comBoCategorieen.FormattingEnabled = true;
			this.comBoCategorieen.Location = new System.Drawing.Point(1035, 74);
			this.comBoCategorieen.Name = "comBoCategorieen";
			this.comBoCategorieen.Size = new System.Drawing.Size(191, 32);
			this.comBoCategorieen.TabIndex = 7;
			this.comBoCategorieen.SelectedIndexChanged += new System.EventHandler(this.comBoCategorieen_SelectedIndexChanged);
			// 
			// btnGereedmelden
			// 
			this.btnGereedmelden.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.btnGereedmelden.Location = new System.Drawing.Point(1070, 608);
			this.btnGereedmelden.Name = "btnGereedmelden";
			this.btnGereedmelden.Size = new System.Drawing.Size(156, 37);
			this.btnGereedmelden.TabIndex = 8;
			this.btnGereedmelden.Text = "Gereedmelden";
			this.btnGereedmelden.UseVisualStyleBackColor = true;
			this.btnGereedmelden.Click += new System.EventHandler(this.btnGereedmelden_Click);
			// 
			// lvBestelItemsInhoudColOpgenomen
			// 
			this.lvBestelItemsInhoudColOpgenomen.Text = "Tijd";
			this.lvBestelItemsInhoudColOpgenomen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.lvBestelItemsInhoudColOpgenomen.Width = 95;
			// 
			// KeukenForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1264, 661);
			this.ControlBox = false;
			this.Controls.Add(this.btnGereedmelden);
			this.Controls.Add(this.comBoCategorieen);
			this.Controls.Add(this.btnLogout);
			this.Controls.Add(this.lblIngelogdeMedewerker);
			this.Controls.Add(this.lvBestelItemsInhoud);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "KeukenForm";
			this.Text = "KeukenForm";
			this.Shown += new System.EventHandler(this.KeukenForm_Shown);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListView lvBestelItemsInhoud;
		private System.Windows.Forms.ColumnHeader lvBestelItemsInhoudBestellingId;
		private System.Windows.Forms.ColumnHeader lvBestelItemsInhoudColMenuItemId;
		private System.Windows.Forms.ColumnHeader lvBestelItemsInhoudColMenuItemAantal;
		private System.Windows.Forms.ColumnHeader lvBestelItemsInhoudColMenuItemNaam;
		private System.Windows.Forms.Button btnLogout;
		private System.Windows.Forms.Label lblIngelogdeMedewerker;
		private System.Windows.Forms.ComboBox comBoCategorieen;
		private System.Windows.Forms.Button btnGereedmelden;
		private System.Windows.Forms.ColumnHeader lvBestelItemsInhoudColTafelNr;
		private System.Windows.Forms.ColumnHeader lvBestelItemsInhoudColOpgenomen;
	}
}