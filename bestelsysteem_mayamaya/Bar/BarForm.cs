﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
	public partial class BarForm : Form
	{
		private BestelSysteem bestelSysteem;

		public BarForm(BestelSysteem bestelSysteem) {
			InitializeComponent();
			
			this.bestelSysteem = bestelSysteem;

			WeergevenBestelItemsBar();
		}

		private void WeergevenBestelItemsBar() {
			List<BestelItem> besteldeItems = bestelSysteem.OphalenBestelItemsBar();
			lvBestellingen.Items.Clear();


			int bestellingId = 0;
			string opgenomenTijd = "";

			//Bestelde menuitems die niet gereedgemeld zijn, ophalen en toevoegen aan bestellingenoverzicht van de bar
			foreach (BestelItem item in besteldeItems) {
				//Groeperen op bestelde menu items
				if (opgenomenTijd == item.Opgenomen.ToString() && bestellingId == item.BestellingId) {
					continue;
				}

				bestellingId = item.BestellingId;
				opgenomenTijd = item.Opgenomen.ToString();

				Bestelling bestelling = bestelSysteem.ZoekBestellingOpBestellingId(item.BestellingId);

				//Zoeken van een tafelNummer bij de bestelling
				int tafelnummer = 0;
				if (bestelling != null) {
					Tafel tafel = bestelSysteem.ZoekTafelOpTafelId(bestelling.TafelId);

					if (tafel != null) {
						tafelnummer = tafel.Nummer;
					}
				}

				//Toon foutmelding indien er geen tafelnummer gevonden kan worden
				if (tafelnummer == 0) {
					MessageBox.Show("Er is een fout opgetreden bij het samenstellen van de bestellingenlijst, probeer het later nogmaals.\nIndien deze fout zich herhaalt, neem dan contact op met uw administrator.", "Systeemfout", MessageBoxButtons.OK, MessageBoxIcon.Error);

					bestelSysteem.Logout();
					this.Close();
				}

				ListViewItem lvItem = new ListViewItem("Tafel " + tafelnummer);
				lvItem.SubItems.Add(item.Opgenomen.ToString("HH:mm"));
				lvItem.SubItems.Add(item.BestellingId.ToString());
				lvItem.SubItems.Add(item.Opgenomen.TimeOfDay.ToString());

				lvBestellingen.Items.Add(lvItem);
			}
		}

		#region Events

		private void btnLogout_Click(object sender, EventArgs e) {
			bestelSysteem.Logout();
			this.Close();
		}

		private void BarForm_Shown(object sender, EventArgs e) {
			lblIngelogdeMedewerker.Text = bestelSysteem.IngelogdeWerknemer.Voornaam + " " + bestelSysteem.IngelogdeWerknemer.Achternaam;
		}

		//toon de bestelde items van een tafel in de linkerkolom
		private void lvBestellingen_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e) {
			List<BestelItem> besteldeItems = bestelSysteem.OphalenBestelItemsBar();
			lvBestelItemsInhoud.Items.Clear();

			//Doe niets als er geen tafel geselecteerd is
			if (lvBestellingen.SelectedItems.Count == 0) {
				return;
			}

			//Ophalen bestellingId en tijd van opnemen
			int bestellingId = Convert.ToInt32(lvBestellingen.SelectedItems[0].SubItems[2].Text);
			string opgenomen = lvBestellingen.SelectedItems[0].SubItems[3].Text;

			foreach (BestelItem item in besteldeItems) {
				if (bestellingId != item.BestellingId || opgenomen != item.Opgenomen.TimeOfDay.ToString()) {
					continue;
				}

				ListViewItem lvItem = new ListViewItem(item.BestellingId.ToString());
				lvItem.SubItems.Add(item.MenuItemId.ToString());
				lvItem.SubItems.Add(item.Aantal.ToString() + "x");
				lvItem.SubItems.Add(bestelSysteem.ZoekMenuItemOpMenuItemId(item.MenuItemId).Naam);

				//Voeg menuItem toe aan het overzicht rechts
				lvBestelItemsInhoud.Items.Add(lvItem);
			}
		}

		private void btnGereedmelden_Click(object sender, EventArgs e) {
			List<BestelItem> besteldeItems = bestelSysteem.OphalenBestelItemsBar();

			//Doe niets als er geen tafel geselecteerd is
			if (lvBestellingen.SelectedItems.Count == 0) {
				return;
			}

			//Ophalen bestellingId en tijd van opnemen
			int bestellingId = Convert.ToInt32(lvBestellingen.SelectedItems[0].SubItems[2].Text);
			string opgenomen = lvBestellingen.SelectedItems[0].SubItems[3].Text;

			//loop door bestelitemsbar
			foreach (BestelItem item in besteldeItems) {
				if (item.BestellingId == bestellingId && item.Opgenomen.TimeOfDay.ToString() == opgenomen) {
					//Meldt gevonden items gereed in DB in list
					bestelSysteem.GereedmeldenBestellingItem(item);
				}
			}

			//Update interface
			lvBestelItemsInhoud.Items.Clear();
			WeergevenBestelItemsBar();
		}
		#endregion
	}
}
