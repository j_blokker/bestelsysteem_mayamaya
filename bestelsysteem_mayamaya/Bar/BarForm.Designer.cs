﻿namespace bestelsysteem_mayamaya
{
	partial class BarForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BarForm));
			this.lblIngelogdeMedewerker = new System.Windows.Forms.Label();
			this.lvBestellingen = new System.Windows.Forms.ListView();
			this.lvBestellingenColTafelNummer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestellingenColOpgenomenDisplay = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestellingenColBestellingId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestellingenColOpgenomen = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestelItemsInhoud = new System.Windows.Forms.ListView();
			this.lvBestelItemsInhoudBestellingId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestelItemsInhoudColMenuItemId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestelItemsInhoudColMenuItemAmount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvBestelItemsInhoudColMenuItemName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.btnLogout = new System.Windows.Forms.Button();
			this.btnGereedmelden = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblIngelogdeMedewerker
			// 
			this.lblIngelogdeMedewerker.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblIngelogdeMedewerker.Location = new System.Drawing.Point(728, 22);
			this.lblIngelogdeMedewerker.Name = "lblIngelogdeMedewerker";
			this.lblIngelogdeMedewerker.Size = new System.Drawing.Size(450, 24);
			this.lblIngelogdeMedewerker.TabIndex = 2;
			this.lblIngelogdeMedewerker.Text = "[Medewerker]";
			this.lblIngelogdeMedewerker.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lvBestellingen
			// 
			this.lvBestellingen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvBestellingenColTafelNummer,
            this.lvBestellingenColOpgenomenDisplay,
            this.lvBestellingenColBestellingId,
            this.lvBestellingenColOpgenomen});
			this.lvBestellingen.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lvBestellingen.FullRowSelect = true;
			this.lvBestellingen.GridLines = true;
			this.lvBestellingen.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvBestellingen.Location = new System.Drawing.Point(36, 74);
			this.lvBestellingen.MultiSelect = false;
			this.lvBestellingen.Name = "lvBestellingen";
			this.lvBestellingen.Size = new System.Drawing.Size(310, 521);
			this.lvBestellingen.TabIndex = 3;
			this.lvBestellingen.TileSize = new System.Drawing.Size(200, 40);
			this.lvBestellingen.UseCompatibleStateImageBehavior = false;
			this.lvBestellingen.View = System.Windows.Forms.View.Details;
			this.lvBestellingen.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lvBestellingen_ItemSelectionChanged);
			// 
			// lvBestellingenColTafelNummer
			// 
			this.lvBestellingenColTafelNummer.Text = "Tafel";
			this.lvBestellingenColTafelNummer.Width = 185;
			// 
			// lvBestellingenColOpgenomenDisplay
			// 
			this.lvBestellingenColOpgenomenDisplay.Text = "Tijd";
			this.lvBestellingenColOpgenomenDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.lvBestellingenColOpgenomenDisplay.Width = 120;
			// 
			// lvBestellingenColBestellingId
			// 
			this.lvBestellingenColBestellingId.Text = "";
			this.lvBestellingenColBestellingId.Width = 0;
			// 
			// lvBestellingenColOpgenomen
			// 
			this.lvBestellingenColOpgenomen.Width = 0;
			// 
			// lvBestelItemsInhoud
			// 
			this.lvBestelItemsInhoud.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvBestelItemsInhoudBestellingId,
            this.lvBestelItemsInhoudColMenuItemId,
            this.lvBestelItemsInhoudColMenuItemAmount,
            this.lvBestelItemsInhoudColMenuItemName});
			this.lvBestelItemsInhoud.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F);
			this.lvBestelItemsInhoud.GridLines = true;
			this.lvBestelItemsInhoud.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.lvBestelItemsInhoud.Location = new System.Drawing.Point(375, 74);
			this.lvBestelItemsInhoud.Name = "lvBestelItemsInhoud";
			this.lvBestelItemsInhoud.Size = new System.Drawing.Size(850, 521);
			this.lvBestelItemsInhoud.TabIndex = 3;
			this.lvBestelItemsInhoud.TileSize = new System.Drawing.Size(212, 40);
			this.lvBestelItemsInhoud.UseCompatibleStateImageBehavior = false;
			this.lvBestelItemsInhoud.View = System.Windows.Forms.View.Details;
			// 
			// lvBestelItemsInhoudBestellingId
			// 
			this.lvBestelItemsInhoudBestellingId.DisplayIndex = 2;
			this.lvBestelItemsInhoudBestellingId.Width = 0;
			// 
			// lvBestelItemsInhoudColMenuItemId
			// 
			this.lvBestelItemsInhoudColMenuItemId.DisplayIndex = 3;
			this.lvBestelItemsInhoudColMenuItemId.Width = 0;
			// 
			// lvBestelItemsInhoudColMenuItemAmount
			// 
			this.lvBestelItemsInhoudColMenuItemAmount.DisplayIndex = 0;
			this.lvBestelItemsInhoudColMenuItemAmount.Text = "Aantal";
			this.lvBestelItemsInhoudColMenuItemAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.lvBestelItemsInhoudColMenuItemAmount.Width = 110;
			// 
			// lvBestelItemsInhoudColMenuItemName
			// 
			this.lvBestelItemsInhoudColMenuItemName.DisplayIndex = 1;
			this.lvBestelItemsInhoudColMenuItemName.Tag = "1";
			this.lvBestelItemsInhoudColMenuItemName.Text = "Naam";
			this.lvBestelItemsInhoudColMenuItemName.Width = 735;
			// 
			// btnLogout
			// 
			this.btnLogout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLogout.BackgroundImage")));
			this.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btnLogout.FlatAppearance.BorderSize = 0;
			this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.btnLogout.Location = new System.Drawing.Point(1181, 12);
			this.btnLogout.Margin = new System.Windows.Forms.Padding(0);
			this.btnLogout.Name = "btnLogout";
			this.btnLogout.Size = new System.Drawing.Size(45, 45);
			this.btnLogout.TabIndex = 4;
			this.btnLogout.UseVisualStyleBackColor = true;
			this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
			// 
			// btnGereedmelden
			// 
			this.btnGereedmelden.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
			this.btnGereedmelden.Location = new System.Drawing.Point(1070, 608);
			this.btnGereedmelden.Name = "btnGereedmelden";
			this.btnGereedmelden.Size = new System.Drawing.Size(156, 37);
			this.btnGereedmelden.TabIndex = 5;
			this.btnGereedmelden.Text = "Gereedmelden";
			this.btnGereedmelden.UseVisualStyleBackColor = true;
			this.btnGereedmelden.Click += new System.EventHandler(this.btnGereedmelden_Click);
			// 
			// BarForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1264, 661);
			this.ControlBox = false;
			this.Controls.Add(this.btnGereedmelden);
			this.Controls.Add(this.btnLogout);
			this.Controls.Add(this.lvBestelItemsInhoud);
			this.Controls.Add(this.lvBestellingen);
			this.Controls.Add(this.lblIngelogdeMedewerker);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "BarForm";
			this.Padding = new System.Windows.Forms.Padding(3);
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Bar";
			this.Shown += new System.EventHandler(this.BarForm_Shown);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label lblIngelogdeMedewerker;
		private System.Windows.Forms.ListView lvBestellingen;
		private System.Windows.Forms.ColumnHeader lvBestellingenColBestellingId;
		private System.Windows.Forms.ListView lvBestelItemsInhoud;
		private System.Windows.Forms.ColumnHeader lvBestelItemsInhoudColMenuItemName;
		private System.Windows.Forms.ColumnHeader lvBestelItemsInhoudColMenuItemAmount;
		private System.Windows.Forms.Button btnLogout;
		private System.Windows.Forms.Button btnGereedmelden;
		private System.Windows.Forms.ColumnHeader lvBestellingenColOpgenomenDisplay;
		private System.Windows.Forms.ColumnHeader lvBestellingenColTafelNummer;
		private System.Windows.Forms.ColumnHeader lvBestelItemsInhoudBestellingId;
		private System.Windows.Forms.ColumnHeader lvBestelItemsInhoudColMenuItemId;
		private System.Windows.Forms.ColumnHeader lvBestellingenColOpgenomen;
	}
}