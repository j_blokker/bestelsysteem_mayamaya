﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
	public partial class LoginForm : Form
	{
		BestelSysteem bestelSysteem;
		Functie vereisteFunctie;

		public LoginForm(BestelSysteem bestelSysteem, Functie functie) {
			InitializeComponent();
            
			this.bestelSysteem = bestelSysteem;

			vereisteFunctie = functie;
		}

		private void btnLogin_Click(object sender, EventArgs e) {

			bestelSysteem.Login(txtboxPincode.Text, vereisteFunctie);
			if (bestelSysteem.IngelogdeWerknemer != null) {
                this.DialogResult = DialogResult.OK;
				this.Close();
			} else {
				MessageBox.Show("Vul een geldige pincode in.", "Toegang geweigerd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtboxPincode.Text = "";
				txtboxPincode.Focus();
			}
		}

		private void btnAnnuleren_Click(object sender, EventArgs e) {
			this.Close();
		}
	}
}
