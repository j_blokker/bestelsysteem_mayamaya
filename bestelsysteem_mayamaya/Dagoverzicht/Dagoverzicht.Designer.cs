﻿namespace bestelsysteem_mayamaya
{
	partial class Dagoverzicht
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dagoverzicht));
            this.comBoTafels = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lvBestellingen = new System.Windows.Forms.ListView();
            this.lvBestellingenColNaam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvBestellingenColId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvBestelItemsInhoud = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvBestellingInhoudColMenuItemAantal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvBestellingInhoudColMenuItemNaam = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvBestellingInhoudColPrijs = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblBestelling = new System.Windows.Forms.Label();
            this.lblTotaalbedrag = new System.Windows.Forms.Label();
            this.btnLogout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comBoTafels
            // 
            this.comBoTafels.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comBoTafels.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.comBoTafels.FormattingEnabled = true;
            this.comBoTafels.Location = new System.Drawing.Point(115, 33);
            this.comBoTafels.Name = "comBoTafels";
            this.comBoTafels.Size = new System.Drawing.Size(191, 32);
            this.comBoTafels.TabIndex = 8;
            this.comBoTafels.SelectedIndexChanged += new System.EventHandler(this.comBoTafels_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label1.Location = new System.Drawing.Point(32, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 24);
            this.label1.TabIndex = 9;
            this.label1.Text = "Toon:";
            // 
            // lvBestellingen
            // 
            this.lvBestellingen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvBestellingenColNaam,
            this.lvBestellingenColId});
            this.lvBestellingen.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvBestellingen.FullRowSelect = true;
            this.lvBestellingen.GridLines = true;
            this.lvBestellingen.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvBestellingen.Location = new System.Drawing.Point(36, 94);
            this.lvBestellingen.MultiSelect = false;
            this.lvBestellingen.Name = "lvBestellingen";
            this.lvBestellingen.Size = new System.Drawing.Size(270, 541);
            this.lvBestellingen.TabIndex = 10;
            this.lvBestellingen.TileSize = new System.Drawing.Size(200, 40);
            this.lvBestellingen.UseCompatibleStateImageBehavior = false;
            this.lvBestellingen.View = System.Windows.Forms.View.Details;
            this.lvBestellingen.SelectedIndexChanged += new System.EventHandler(this.lvBestellingen_SelectedIndexChanged);
            // 
            // lvBestellingenColNaam
            // 
            this.lvBestellingenColNaam.Width = 265;
            // 
            // lvBestellingenColId
            // 
            this.lvBestellingenColId.Width = 0;
            // 
            // lvBestelItemsInhoud
            // 
            this.lvBestelItemsInhoud.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.lvBestellingInhoudColMenuItemAantal,
            this.lvBestellingInhoudColMenuItemNaam,
            this.lvBestellingInhoudColPrijs});
            this.lvBestelItemsInhoud.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvBestelItemsInhoud.FullRowSelect = true;
            this.lvBestelItemsInhoud.GridLines = true;
            this.lvBestelItemsInhoud.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvBestelItemsInhoud.Location = new System.Drawing.Point(356, 142);
            this.lvBestelItemsInhoud.Name = "lvBestelItemsInhoud";
            this.lvBestelItemsInhoud.Size = new System.Drawing.Size(875, 432);
            this.lvBestelItemsInhoud.TabIndex = 11;
            this.lvBestelItemsInhoud.TileSize = new System.Drawing.Size(212, 40);
            this.lvBestelItemsInhoud.UseCompatibleStateImageBehavior = false;
            this.lvBestelItemsInhoud.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 0;
            // 
            // lvBestellingInhoudColMenuItemAantal
            // 
            this.lvBestellingInhoudColMenuItemAantal.Text = "Aantal";
            this.lvBestellingInhoudColMenuItemAantal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lvBestellingInhoudColMenuItemAantal.Width = 90;
            // 
            // lvBestellingInhoudColMenuItemNaam
            // 
            this.lvBestellingInhoudColMenuItemNaam.Tag = "";
            this.lvBestellingInhoudColMenuItemNaam.Text = "Naam";
            this.lvBestellingInhoudColMenuItemNaam.Width = 560;
            // 
            // lvBestellingInhoudColPrijs
            // 
            this.lvBestellingInhoudColPrijs.Text = "Prijs";
            this.lvBestellingInhoudColPrijs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.lvBestellingInhoudColPrijs.Width = 220;
            // 
            // lblBestelling
            // 
            this.lblBestelling.AutoSize = true;
            this.lblBestelling.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBestelling.Location = new System.Drawing.Point(365, 94);
            this.lblBestelling.Name = "lblBestelling";
            this.lblBestelling.Size = new System.Drawing.Size(227, 29);
            this.lblBestelling.TabIndex = 9;
            this.lblBestelling.Text = "Bestelling [bestelnr]";
            // 
            // lblTotaalbedrag
            // 
            this.lblTotaalbedrag.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblTotaalbedrag.Location = new System.Drawing.Point(753, 606);
            this.lblTotaalbedrag.Name = "lblTotaalbedrag";
            this.lblTotaalbedrag.Size = new System.Drawing.Size(475, 29);
            this.lblTotaalbedrag.TabIndex = 9;
            this.lblTotaalbedrag.Text = "Totaalbedrag:  €123,45";
            this.lblTotaalbedrag.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnLogout
            // 
            this.btnLogout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLogout.BackgroundImage")));
            this.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnLogout.Location = new System.Drawing.Point(1186, 33);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(0);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(45, 45);
            this.btnLogout.TabIndex = 12;
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // Dagoverzicht
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 661);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.lvBestelItemsInhoud);
            this.Controls.Add(this.lvBestellingen);
            this.Controls.Add(this.lblTotaalbedrag);
            this.Controls.Add(this.lblBestelling);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comBoTafels);
            this.Name = "Dagoverzicht";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dagoverzicht";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox comBoTafels;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListView lvBestellingen;
		private System.Windows.Forms.ColumnHeader lvBestellingenColNaam;
		private System.Windows.Forms.ListView lvBestelItemsInhoud;
		private System.Windows.Forms.ColumnHeader lvBestellingInhoudColMenuItemAantal;
		private System.Windows.Forms.ColumnHeader lvBestellingInhoudColMenuItemNaam;
		private System.Windows.Forms.ColumnHeader lvBestellingInhoudColPrijs;
		private System.Windows.Forms.Label lblBestelling;
		private System.Windows.Forms.Label lblTotaalbedrag;
		private System.Windows.Forms.Button btnLogout;
		private System.Windows.Forms.ColumnHeader lvBestellingenColId;
		private System.Windows.Forms.ColumnHeader columnHeader1;
	}
}