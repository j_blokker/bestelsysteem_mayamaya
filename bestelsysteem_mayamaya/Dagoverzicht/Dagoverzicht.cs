﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace bestelsysteem_mayamaya
{
	public partial class Dagoverzicht : Form
	{
		BestelSysteem bestelSysteem;

		public Dagoverzicht(BestelSysteem bestelSysteem) {
			InitializeComponent();
			this.bestelSysteem = bestelSysteem;

			//Vullen van dropdown met tafels
			comBoTafels.Items.Add("Alles");
			foreach(Tafel tafel in bestelSysteem.Tafels){
				comBoTafels.Items.Add(tafel);
			}
			comBoTafels.SelectedIndex = 0;
			lblBestelling.Text = "";

		}

		private void btnLogout_Click(object sender, EventArgs e) {
            this.Close();
		}

		private void comBoTafels_SelectedIndexChanged(object sender, EventArgs e) {
			List<Bestelling> gefilterdeBestellingen = new List<Bestelling>();
			List<Bestelling> bestellingen = bestelSysteem.OphalenDagBestellingen();
			
			lblTotaalbedrag.Text = "";
			lblBestelling.Text = "";
			lvBestellingen.Items.Clear();
			lvBestelItemsInhoud.Items.Clear();

			//Wanneer geselecteerde optie 'alles' betreft toon alles.
			//Anders alleen de bestellingen weergeven voor de geselecteerde tafel
			if(comBoTafels.SelectedItem is string){
				foreach (Bestelling bestelling in bestellingen) {
					gefilterdeBestellingen.Add(bestelling);
				}
			}
			else {
				Tafel geselecteerdeTafel = (Tafel)comBoTafels.SelectedItem;

				//Filteren van bestellingen op tafelId
				foreach (Bestelling bestelling in bestellingen) {
					if (bestelling.TafelId == geselecteerdeTafel.Id && bestelling.Afgerond) {
						gefilterdeBestellingen.Add(bestelling);
					}
				}
			}

			//Vullen van bestellingoverzicht met de gefilterde bestellingen
			foreach (Bestelling gefilterdeBestelling in gefilterdeBestellingen) {
				ListViewItem lvItem = new ListViewItem("Bestelling " + gefilterdeBestelling.Id);
				lvItem.SubItems.Add(gefilterdeBestelling.Id.ToString());

				lvBestellingen.Items.Add(lvItem);
			}
		}

		private void lvBestellingen_SelectedIndexChanged(object sender, EventArgs e) {
			//Reset
			lvBestelItemsInhoud.Items.Clear();
			lblBestelling.Text = "";
			lblTotaalbedrag.Text = "";

			//Doe niets wanneer er niets geselecteerd is
			if(lvBestellingen.SelectedItems.Count == 0){
				return;
			}

			//Ophalen bestelling via geselecteerde bestelling
			int bestellingId = Int32.Parse(lvBestellingen.SelectedItems[0].SubItems[1].Text);
			Bestelling bestelling = bestelSysteem.ZoekBestellingOpBestellingId(bestellingId);
			List<BestelItem> besteldeItems = bestelSysteem.BestelItems;

			//Invullen van detailview
			lblBestelling.Text = "Bestelling " + bestelling.Id;

			foreach(BestelItem bItem in besteldeItems){
				if (bItem.BestellingId == bestelling.Id) {
					MenuItem mItem = bestelSysteem.ZoekMenuItemOpMenuItemId(bItem.MenuItemId);

					ListViewItem lvItem = new ListViewItem();
					lvItem.SubItems.Add(bItem.Aantal.ToString() + "x");
					lvItem.SubItems.Add(mItem.Naam);
					lvItem.SubItems.Add((mItem.Prijs * bItem.Aantal).ToString("C", new CultureInfo("nl-NL")));

					lvBestelItemsInhoud.Items.Add(lvItem);
				}
			}

			//Fooi toevoegen
			if (bestelling.Fooi > 0) {
				ListViewItem lvItem = new ListViewItem();
				lvItem.SubItems.Add("");
				lvItem.SubItems.Add("Fooi");
				lvItem.SubItems.Add(bestelling.Fooi.ToString("C", new CultureInfo("nl-NL")));
				lvBestelItemsInhoud.Items.Add(lvItem);
			}

			//Totaalbedrag tonen
			lblTotaalbedrag.Text = "Totaalbedrag:  " + bestelling.Totaalbedrag.ToString("C", new CultureInfo("nl-NL"));
		}
	}
}
