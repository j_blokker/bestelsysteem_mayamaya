﻿public enum Functie
{
	Management = 1,
	Bediening,
	Bar,
	Keuken
}

public enum Menukaart
{
	Dranken = 1,
	Lunch,
	Diner
}