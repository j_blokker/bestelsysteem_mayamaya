﻿namespace bestelsysteem_mayamaya
{
	partial class HoofdmenuForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.buttonBar = new System.Windows.Forms.Button();
			this.buttonBediening = new System.Windows.Forms.Button();
			this.buttonKeuken = new System.Windows.Forms.Button();
			this.buttonManagement = new System.Windows.Forms.Button();
			this.buttonDagoverzicht = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// buttonBar
			// 
			this.buttonBar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonBar.Location = new System.Drawing.Point(375, 85);
			this.buttonBar.Name = "buttonBar";
			this.buttonBar.Size = new System.Drawing.Size(505, 74);
			this.buttonBar.TabIndex = 0;
			this.buttonBar.Text = "Bar";
			this.buttonBar.UseVisualStyleBackColor = true;
			this.buttonBar.Click += new System.EventHandler(this.buttonBar_Click);
			// 
			// buttonBediening
			// 
			this.buttonBediening.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonBediening.Location = new System.Drawing.Point(375, 187);
			this.buttonBediening.Name = "buttonBediening";
			this.buttonBediening.Size = new System.Drawing.Size(505, 74);
			this.buttonBediening.TabIndex = 0;
			this.buttonBediening.Text = "Bediening";
			this.buttonBediening.UseVisualStyleBackColor = true;
			this.buttonBediening.Click += new System.EventHandler(this.buttonBediening_Click);
			// 
			// buttonKeuken
			// 
			this.buttonKeuken.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonKeuken.Location = new System.Drawing.Point(375, 290);
			this.buttonKeuken.Name = "buttonKeuken";
			this.buttonKeuken.Size = new System.Drawing.Size(505, 74);
			this.buttonKeuken.TabIndex = 0;
			this.buttonKeuken.Text = "Keuken";
			this.buttonKeuken.UseVisualStyleBackColor = true;
			this.buttonKeuken.Click += new System.EventHandler(this.buttonKeuken_Click);
			// 
			// buttonManagement
			// 
			this.buttonManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonManagement.Location = new System.Drawing.Point(375, 393);
			this.buttonManagement.Name = "buttonManagement";
			this.buttonManagement.Size = new System.Drawing.Size(505, 74);
			this.buttonManagement.TabIndex = 0;
			this.buttonManagement.Text = "Management";
			this.buttonManagement.UseVisualStyleBackColor = true;
			this.buttonManagement.Click += new System.EventHandler(this.buttonManagement_Click);
			// 
			// buttonDagoverzicht
			// 
			this.buttonDagoverzicht.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.buttonDagoverzicht.Location = new System.Drawing.Point(375, 496);
			this.buttonDagoverzicht.Name = "buttonDagoverzicht";
			this.buttonDagoverzicht.Size = new System.Drawing.Size(505, 74);
			this.buttonDagoverzicht.TabIndex = 0;
			this.buttonDagoverzicht.Text = "Dagoverzicht";
			this.buttonDagoverzicht.UseVisualStyleBackColor = true;
			this.buttonDagoverzicht.Click += new System.EventHandler(this.buttonDagoverzicht_Click);
			// 
			// HoofdmenuForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1264, 661);
			this.ControlBox = false;
			this.Controls.Add(this.buttonDagoverzicht);
			this.Controls.Add(this.buttonManagement);
			this.Controls.Add(this.buttonKeuken);
			this.Controls.Add(this.buttonBediening);
			this.Controls.Add(this.buttonBar);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "HoofdmenuForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Bestelsysteem MayaMaya";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button buttonBar;
		private System.Windows.Forms.Button buttonBediening;
		private System.Windows.Forms.Button buttonKeuken;
		private System.Windows.Forms.Button buttonManagement;
		private System.Windows.Forms.Button buttonDagoverzicht;
	}
}

