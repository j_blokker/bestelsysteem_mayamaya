﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace bestelsysteem_mayamaya
{
    public class BestelSysteem
    {
        /* Read-only properties */
		public Werknemer IngelogdeWerknemer { get; private set; }
		public List<BestelItem> BestelItems { get; private set; }
		public List<MenuItem> MenuItems { get; private set; }
		public List<Categorie> Categorieen { get; private set; }
		public List<Bestelling> Bestellingen { get; private set; }
		public List<Werknemer> Werknemers { get; private set; }
		public List<Tafel> Tafels { get; private set; }

        public BestelSysteem()
        {
            OphalenWerknemers();
            OphalenMenuItems();
            OphalenBestellingen();
            OphalenbesteldeMenuItems();
            OphalenTafels();
            OphalenCategorieen();
        }

        #region Gegevens ophalen uit Database

        /* Uitlezen alle Werknemers en omzetten naar een List */
        private void OphalenWerknemers()
        {
            Werknemers = new List<Werknemer>();

            DataTable result;
            string query = @"
				SELECT werknemer.*, functie.naam AS functie_naam 
				FROM werknemer 
				INNER JOIN functie ON werknemer.functie_id = functie.Id
			";

            using (SqlCommand cmd = new SqlCommand(query))
            {
                result = Database.Request(cmd);
            }

            if (result != null)
            {
                foreach (DataRow record in result.Rows)
                {
                    Werknemers.Add(
                        new Werknemer(
                            (int)record["id"],
                            (string)record["voornaam"],
                            (string)record["achternaam"],
                            (string)record["pincode"],
                            (Functie)record["functie_id"]
                        )
                    );
                }
            }
        }

        /* Haalt alle menukaart items op */
        private void OphalenMenuItems()
        {
            MenuItems = new List<MenuItem>();

            DataTable result;
            string query = "SELECT * FROM menuitem";

            using (SqlCommand cmd = new SqlCommand(query))
            {
                result = Database.Request(cmd);
            }

            if (result != null)
            {
                foreach (DataRow record in result.Rows)
                {
                    MenuItems.Add(
                        new MenuItem(
                            (int)record["id"],
                            (int)record["btw_id"],
                            (int)record["categorie_id"],
                            (int)record["menukaart_id"],
                            (string)record["naam"],
                            Convert.ToDouble(record["prijs"]),
                            (string)record["omschrijving"]
                        )
                    );
                }
            }
        }

        /* Haal alle Bestellingen op */
        private void OphalenBestellingen()
        {
            Bestellingen = new List<Bestelling>();

            DataTable result;

            string query = "SELECT * FROM bestelling";

            using (SqlCommand cmd = new SqlCommand(query))
            {
                result = Database.Request(cmd);
            }

            if (result != null)
            {
                foreach (DataRow record in result.Rows)
                {
                    Bestellingen.Add(
                        new Bestelling(
                            (int)record["id"],
                            (int)record["tafel_id"],
                            (int)record["werknemer_id"],
                            (bool)record["afgerond"],
                            (record["totaalbedrag"] != System.DBNull.Value) ? Convert.ToDouble(record["totaalbedrag"]) : 0,
                            (record["fooi"] != System.DBNull.Value) ? Convert.ToDouble(record["fooi"]) : 0,
                            (record["opmerking"] != System.DBNull.Value) ? (string)record["opmerking"] : ""
                        )
                    );
                }
            }
        }

        /* Haal alle afgeronde Bestellingen op van de huidige dag */
        public List<Bestelling> OphalenDagBestellingen()
        {
            List<Bestelling> bestellingen = new List<Bestelling>();

            DataTable result;

            string query = @"
				SELECT * FROM bestelling
				INNER JOIN bestelling_menuitem ON bestelling.id = bestelling_menuitem.bestelling_id
				WHERE afgerond = 1
				AND opgenomen >= CONVERT(date, GETDATE())
				AND opgenomen < DATEADD(day, 1, CONVERT(date, GETDATE()))
				ORDER BY opgenomen";

            using (SqlCommand cmd = new SqlCommand(query))
            {
                result = Database.Request(cmd);
            }

            if (result != null)
            {
				int prevBestellingId = 0;

				//Toevoegen van unieke bestellingen van de huidige dag
                foreach (DataRow record in result.Rows)
                {
                    if ((int)record["id"] == prevBestellingId)
					{
                        continue;
                    }

					prevBestellingId = (int)record["id"];

					bestellingen.Add(
                        new Bestelling(
                            (int)record["id"],
                            (int)record["tafel_id"],
                            (int)record["werknemer_id"],
                            (bool)record["afgerond"],
                            (record["totaalbedrag"] != System.DBNull.Value) ? Convert.ToDouble(record["totaalbedrag"]) : 0,
                            (record["fooi"] != System.DBNull.Value) ? Convert.ToDouble(record["fooi"]) : 0,
                            (record["opmerking"] != System.DBNull.Value) ? (string)record["opmerking"] : ""
                        )
                    );
                }
            }

			return bestellingen;
        }

        /* Haal alle bestelde items op die nog niet afgemeld zijn, vandaag geplaatst zijn en sorteer deze op afneemtijd */
        private void OphalenbesteldeMenuItems()
        {
            BestelItems = new List<BestelItem>();

            DataTable result;

            string query = @"
				SELECT * FROM bestelling_menuitem 
				WHERE opgenomen >= CONVERT(date, GETDATE())
				AND opgenomen < DATEADD(day, 1, CONVERT(date, GETDATE()))
				ORDER BY opgenomen
			";

            using (SqlCommand cmd = new SqlCommand(query))
            {
                result = Database.Request(cmd);
            }

            if (result != null)
            {
                foreach (DataRow record in result.Rows)
                {
                    BestelItem item = new BestelItem(
                        (int)record["bestelling_id"],
                        (int)record["menuitem_id"],
                        (int)record["aantal"],
                        (int)record["gereedgemeld"],
                        (DateTime)record["opgenomen"]
                    );

                    BestelItems.Add(item);
                }
            }
        }

        /* Haal alle Tafels op */
        private void OphalenTafels()
        {
            Tafels = new List<Tafel>();

            DataTable result;

            string query = "SELECT * FROM tafel";

            using (SqlCommand cmd = new SqlCommand(query))
            {
                result = Database.Request(cmd);
            }

            if (result != null)
            {
                foreach (DataRow record in result.Rows)
                {
                    Tafels.Add(
                        new Tafel(
                            (int)record["id"],
                            (int)record["nummer"],
                            (int)record["max_personen"],
                            (bool)record["bezet"]
                        )
                    );
                }
            }
        }

        /* Haal alle categorieen op */
        private void OphalenCategorieen()
        {
            Categorieen = new List<Categorie>();

            DataTable result;

            string query = "SELECT * FROM categorie";

            using (SqlCommand cmd = new SqlCommand(query))
            {
                result = Database.Request(cmd);
            }

            if (result != null)
            {
                foreach (DataRow record in result.Rows)
                {
                    Categorieen.Add(
                        new Categorie(
                            (int)record["id"],
                            (string)record["naam"]
                        )
                    );
                }
            }
        }

        #endregion

        /* Inloggen als overig personeelslid */
        public void Login(string pincode, Functie functie)
        {
			Logout();

            foreach (Werknemer werknemer in Werknemers)
            {
                if (werknemer.Pincode == pincode && werknemer.Functie == functie)
                {
                    IngelogdeWerknemer = werknemer;
                    return;
                }
            }
        }

        /* Inloggen als manager */
        public void LoginManager(string gebruikersnaam, string wachtwoord)
        {
			Logout();

            foreach (Werknemer werknemer in Werknemers)
            {
                if (werknemer.Voornaam == gebruikersnaam && werknemer.Pincode == wachtwoord && werknemer.Functie == Functie.Management)
                {
                    IngelogdeWerknemer = werknemer;
                    return;
                }
            }
        }

        /* Uitloggen */
        public void Logout()
        {
            IngelogdeWerknemer = null;
        }

        /* Gereedmelden van een bestelde menuItem, Vervolgens DB en list updaten */
        public bool GereedmeldenBestellingItem(BestelItem item)
        {
            string query = "UPDATE bestelling_menuitem SET gereedgemeld = @gereedgemeld WHERE bestelling_id = @bestellingId AND menuitem_id = @menuItemId";

            bool result = false;
            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.Parameters.AddWithValue("@bestellingId", item.BestellingId);
                cmd.Parameters.AddWithValue("@menuItemId", item.MenuItemId);
                cmd.Parameters.AddWithValue("@gereedgemeld", item.Aantal);

                result = Database.Execute(cmd);
            }

			if (!result) 
			{
				return false;
			}

            //lijst bijwerken
            foreach (BestelItem bestelItem in BestelItems)
            {
                if (bestelItem.BestellingId == item.BestellingId && bestelItem.MenuItemId == item.MenuItemId)
                {
                    bestelItem.Gereedgemeld = bestelItem.Aantal;
                }
            }

			return true;
        }

        /* Betalen van bestelling */
        public bool BetalenBestellingFactuur(Bestelling bestelling, int bestellingId)
        {
            string query = "UPDATE bestelling SET afgerond = @afgerond WHERE id = @bestellingId";
            bool result = false;

            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.Parameters.AddWithValue("@bestellingId", bestellingId);
                cmd.Parameters.AddWithValue("@afgerond", true);

                result = Database.Execute(cmd);
            }

            if (!result)
            {
				return false;
            }

			//lijst bijwerken
			foreach (Bestelling item in Bestellingen) {
				if (bestellingId == item.Id) {
					item.Afgerond = true;
				}
			}

			return true;
        }

        /* Haal alle bestelde MenuItems op voor de Bar */
        public List<BestelItem> OphalenBestelItemsBar()
        {
            List<BestelItem> items = new List<BestelItem>();

            foreach (BestelItem item in BestelItems)
            {
                if (ZoekMenuItemOpMenuItemId(item.MenuItemId).MenukaartId == (int)Menukaart.Dranken && item.Gereedgemeld < item.Aantal)
                {
                    items.Add(item);
                }
            }

            return items;
        }

        /* Haal alle bestelde MenuItems op voor de keuken */
        public List<BestelItem> OphalenBestelItemsKeuken()
        {
            List<BestelItem> items = new List<BestelItem>();

            foreach (BestelItem item in BestelItems)
            {
                if (ZoekMenuItemOpMenuItemId(item.MenuItemId).MenukaartId != (int)Menukaart.Dranken && item.Gereedgemeld < item.Aantal)
                {
                    items.Add(item);
                }
            }

            return items;
        }

        /* Zoek een bestelling op ID */
        public Bestelling ZoekBestellingOpBestellingId(int bestellingId)
        {
            foreach (Bestelling bestelling in Bestellingen)
            {
                if (bestelling.Id == bestellingId)
                {
                    return bestelling;
                }
            }

            return null;
        }

        /* Zoek bestelling op tafel ID */
        public Bestelling ZoekBestellingOpTafelId(int tafelId)
        {
            foreach (Bestelling bestelling in Bestellingen)
            {
                if (bestelling.TafelId == tafelId)
                {
                    return bestelling;
                }
            }

            return null;
        }

		/* Ziek de bestelling, die nog niet afgerond is, bij een specifieke tafel */
        public Bestelling ZoekLopendeBestellingBijTafel(int tafelId)
        {
            foreach (Bestelling bestelling in Bestellingen)
            {
                if (bestelling.TafelId == tafelId && bestelling.Afgerond == false)
                {
                    return bestelling;
                }
            }

            return null;
        }

        /* Zoek een tafel op ID */
        public Tafel ZoekTafelOpTafelId(int tafelId)
        {
            foreach (Tafel tafel in Tafels)
            {
                if (tafel.Id == tafelId)
                {
                    return tafel;
                }
            }

            return null;
        }

        /* Zoek een tafel op tafelNummer */
        public Tafel ZoekTafelOpTafelNummer(int tafelNummer)
        {
            foreach (Tafel tafel in Tafels)
            {
                if (tafel.Nummer == tafelNummer)
                {
                    return tafel;
                }
            }

            return null;
        }

        /* Zoek een menu item op ID */
        public MenuItem ZoekMenuItemOpMenuItemId(int menuItemId)
        {
            foreach (MenuItem item in MenuItems)
            {
                if (item.Id == menuItemId)
                {
                    return item;
                }
            }

            return null;
        }

        /* Pas de opmerking aan */
        public bool OpmerkingAanpassen(int bestellingId, string txtOpmerking)
        {
            string query = "UPDATE bestelling SET opmerking = @txtOpmerking WHERE id = @bestellingId ";

            bool result = false;
            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.Parameters.AddWithValue("@bestellingId", bestellingId);
                cmd.Parameters.AddWithValue("@txtOpmerking", txtOpmerking);

                result = Database.Execute(cmd);
            }

            if (!result)
            {
                return false;
            }

            //update list
            foreach (Bestelling bestelling in Bestellingen)
            {
                if (bestelling.Id == bestellingId)
                {
                    bestelling.Opmerking = txtOpmerking;
                }
            }

            return result;
        }

        /*Haal medewerker data op */
        public Werknemer ZoekWerknemerOpId(int personeelsID)
        {
            foreach (Werknemer werknemer in Werknemers)
            {
                if (werknemer.Id == personeelsID)
                {
                    return werknemer;
                }
            }
            return null;
        }

        /* verwijder werknemer via ID*/
        public bool VerwijderWerknemer(int personeelsID)
        {
            string query = "DELETE FROM werknemer WHERE id = @personeelsID";
            bool result = false;
            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.Parameters.AddWithValue("@personeelsID", personeelsID);
                result = Database.Execute(cmd);
            }

            if (!result)
            {
                return false;
            }

            for (int i = Werknemers.Count - 1; i > 0; i--)
            {
                if (Werknemers[i].Id == personeelsID)
                {
                    Werknemers.Remove(Werknemers[i]);
                    break;
                }
            }

            return result;
        }

		/* Wijzig een werknemer */
        public bool WijzigWerknemer(Werknemer werknemer)
        {
            string query = @"   UPDATE  werknemer
                                SET     functie_id = @functieId,
                                        voornaam = @voornaam,
                                        achternaam = @achternaam,
                                        pincode = @wachtwoord
                                WHERE   id = @personeelsID";
            bool result = false;

            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.Parameters.AddWithValue("@personeelsID", werknemer.Id);
                cmd.Parameters.AddWithValue("@functieId", (int)werknemer.Functie);
                cmd.Parameters.AddWithValue("@voornaam", werknemer.Voornaam);
                cmd.Parameters.AddWithValue("@achternaam", werknemer.Achternaam);
                cmd.Parameters.AddWithValue("@wachtwoord", werknemer.Pincode);
                
				result = Database.Execute(cmd);
            }

			if(!result)
			{
				return false;
			}

            //Lijst bijwerken
            Werknemer target = ZoekWerknemerOpId(werknemer.Id);
			
            if (target == null)
            {
                return false;
            }

            target = werknemer;

            return result;
        }

		/* Voeg een nieuwe werknemer toe */
        public bool NieuwWerknemer(Werknemer werknemer)
        {
            string query = @"   INSERT INTO werknemer (functie_id, voornaam, achternaam, pincode)
                                VALUES  (@functieId, @voornaam, @achternaam, @wachtwoord )";

            int nieuwId = 0;
            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.Parameters.AddWithValue("@functieId", (int)werknemer.Functie);
                cmd.Parameters.AddWithValue("@voornaam", werknemer.Voornaam);
                cmd.Parameters.AddWithValue("@achternaam", werknemer.Achternaam);
                cmd.Parameters.AddWithValue("@wachtwoord", werknemer.Pincode);
                
				nieuwId = Database.Insert(cmd);
            }

            if (nieuwId == 0)
            {
                return false;
            }

            //Lijst bijwerken
            Werknemer nieuwWerknemer = new Werknemer(nieuwId, werknemer.Voornaam, werknemer.Achternaam, werknemer.Pincode, werknemer.Functie);
            Werknemers.Add(nieuwWerknemer);

            return true;
        }

        /* Maak nieuwe bestelling aan voor een tafel en geef de nieuwe bestelling ID terug. */
        public int AanmakenNieuweBestelling(int tafelId)
        {
            int bestellingId;
            string query = @"
			INSERT INTO bestelling (tafel_id, werknemer_id, afgerond, totaalbedrag, fooi,opmerking)
            VALUES(@tafelid, @werknemerid, @afgerond, @totaalbedrag, @fooi, @opmerking)
			";

            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.Parameters.AddWithValue("@tafelid", tafelId);
                cmd.Parameters.AddWithValue("@werknemerid", IngelogdeWerknemer.Id);
                cmd.Parameters.AddWithValue("@afgerond", 0);
                cmd.Parameters.AddWithValue("@totaalbedrag", DBNull.Value);
                cmd.Parameters.AddWithValue("@fooi", DBNull.Value);
                cmd.Parameters.AddWithValue("@opmerking", DBNull.Value);

                bestellingId = Database.Insert(cmd);

            }

            if (bestellingId == 0)
            {
                return 0;
            }
            Bestelling bestelling = new Bestelling(bestellingId, tafelId, IngelogdeWerknemer.Id, false, 0, 0, "");

            Bestellingen.Add(bestelling);

            return bestellingId;
        }

        /* plaats bestelling via tablet */
        public bool InvoerenBesteldeItem(int bestellingId, BestelItem item, DateTime opgenomenTijd)
        {
            string query = @"	INSERT INTO bestelling_menuitem (bestelling_id, menuitem_id, aantal, gereedgemeld, opgenomen)
								VALUES( @bestellingid, @menuitemid, @aantal, @gereedgemeld, @opgenomen)";

            bool result = false;
            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.Parameters.AddWithValue("@bestellingid", bestellingId);
                cmd.Parameters.AddWithValue("@menuitemid", item.MenuItemId);
                cmd.Parameters.AddWithValue("@aantal", item.Aantal);
                cmd.Parameters.AddWithValue("@gereedgemeld", false);
                cmd.Parameters.AddWithValue("@opgenomen", opgenomenTijd);

                //Kan geen id terug verwachten, maar willen wel weten of het gelukt is
                result = Database.Execute(cmd);
            }

            if (!result)
            {
                return false;
            }

            //Lijst bijwerken
            BestelItem bestelItem = new BestelItem(bestellingId, item.MenuItemId, item.Aantal, 0, opgenomenTijd);
            BestelItems.Add(bestelItem);

            return true;
        }

        /*Verwijder een bestelde item van de bestelling*/
        public bool VerwijderenBesteldeItem(int bestellingId, BestelItem item)
        {
            string query = "DELETE FROM bestelling_menuitem WHERE bestelling_id = @bestellingId AND menuitem_id = @menuItemId";

            bool result = false;
            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.Parameters.AddWithValue("@bestellingId", bestellingId);
                cmd.Parameters.AddWithValue("@menuItemId", item.MenuItemId);

                result = Database.Execute(cmd);
            }

            if (!result)
            {
                return false;
            }

            //Lijst bijwerken
            foreach (BestelItem bItem in BestelItems)
            {
                if (bestellingId == bItem.BestellingId && item.MenuItemId == bItem.MenuItemId)
                {
                    BestelItems.Remove(bItem);
                    break;
                }
            }

            return true;
        }

        /*Wijzig het aantal van de bestelde item van een bestelling */
        public bool WijzigenBesteldeItem(int bestellingId, BestelItem item, DateTime opgenomenTijd)
        {
            string query = @"	UPDATE	bestelling_menuitem 
								SET		aantal = @aantal,
										opgenomen = @opgenomen
								WHERE	bestelling_id = @bestellingId AND menuitem_id = @menuItemId";

            bool result = false;
            using (SqlCommand cmd = new SqlCommand(query))
            {
                cmd.Parameters.AddWithValue("@aantal", item.Aantal);
                cmd.Parameters.AddWithValue("@opgenomen", opgenomenTijd);
                cmd.Parameters.AddWithValue("@bestellingId", bestellingId);
                cmd.Parameters.AddWithValue("@menuItemId", item.MenuItemId);

                result = Database.Execute(cmd);
            }

            if (!result)
            {
                return false;
            }

            //Lijst bijwerken
            foreach (BestelItem bItem in BestelItems)
            {
                if (bestellingId == bItem.BestellingId && item.MenuItemId == bItem.MenuItemId)
                {
                    bItem.Aantal = item.Aantal;
                    bItem.Gereedgemeld = (bItem.Gereedgemeld > item.Aantal) ? item.Aantal : bItem.Gereedgemeld;
					break;
                }
            }

            return true;
        }
    }
}
