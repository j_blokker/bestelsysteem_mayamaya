﻿namespace bestelsysteem_mayamaya
{
	partial class LoginForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblPincode = new System.Windows.Forms.Label();
            this.txtboxPincode = new System.Windows.Forms.TextBox();
            this.btnAnnuleren = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnLogin.Location = new System.Drawing.Point(246, 128);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(109, 37);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lblPincode
            // 
            this.lblPincode.AutoSize = true;
            this.lblPincode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPincode.Location = new System.Drawing.Point(115, 75);
            this.lblPincode.Name = "lblPincode";
            this.lblPincode.Size = new System.Drawing.Size(85, 24);
            this.lblPincode.TabIndex = 4;
            this.lblPincode.Text = "Pincode:";
            // 
            // txtboxPincode
            // 
            this.txtboxPincode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtboxPincode.Location = new System.Drawing.Point(206, 72);
            this.txtboxPincode.MaxLength = 4;
            this.txtboxPincode.Name = "txtboxPincode";
            this.txtboxPincode.PasswordChar = '*';
            this.txtboxPincode.ShortcutsEnabled = false;
            this.txtboxPincode.Size = new System.Drawing.Size(149, 29);
            this.txtboxPincode.TabIndex = 3;
            this.txtboxPincode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnAnnuleren
            // 
            this.btnAnnuleren.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAnnuleren.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnAnnuleren.Location = new System.Drawing.Point(119, 128);
            this.btnAnnuleren.Name = "btnAnnuleren";
            this.btnAnnuleren.Size = new System.Drawing.Size(109, 37);
            this.btnAnnuleren.TabIndex = 5;
            this.btnAnnuleren.Text = "Annuleren";
            this.btnAnnuleren.UseVisualStyleBackColor = true;
            this.btnAnnuleren.Click += new System.EventHandler(this.btnAnnuleren_Click);
            // 
            // LoginForm
            // 
            this.AcceptButton = this.btnLogin;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnAnnuleren;
            this.ClientSize = new System.Drawing.Size(464, 249);
            this.ControlBox = false;
            this.Controls.Add(this.btnAnnuleren);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.lblPincode);
            this.Controls.Add(this.txtboxPincode);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Voer uw pincode in";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnLogin;
		private System.Windows.Forms.Label lblPincode;
		private System.Windows.Forms.TextBox txtboxPincode;
		private System.Windows.Forms.Button btnAnnuleren;
	}
}