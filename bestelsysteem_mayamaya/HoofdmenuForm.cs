﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
	public partial class HoofdmenuForm : Form
	{
		public BestelSysteem bestelSysteem;

		public HoofdmenuForm() {
			InitializeComponent();
			bestelSysteem = new BestelSysteem();

            //opent de tablet
			TabletForm_Login tabletLoginForm = new TabletForm_Login(bestelSysteem);
			tabletLoginForm.Show();
		}

		//Toon het loginscherm, nadat een optie van het hoofdmenu geselecteerd is
		private void ShowLoginForm(Form form, Functie functie) {
			LoginForm loginForm = new LoginForm(bestelSysteem, functie);
			form.FormClosing += delegate { this.Show(); };

			DialogResult result = loginForm.ShowDialog();
			if (result == DialogResult.OK) {
				form.Show();
				this.Hide();
			}
		}

		#region Events
		private void buttonBar_Click(object sender, EventArgs e) {
			BarForm form = new BarForm(bestelSysteem);
			ShowLoginForm(form, Functie.Bar);
		}

        private void buttonBediening_Click(object sender, EventArgs e)
        {
            TafelOverzicht form = new TafelOverzicht(bestelSysteem);
            ShowLoginForm(form, Functie.Bediening);
        }

		private void buttonKeuken_Click(object sender, EventArgs e) {
			KeukenForm form = new KeukenForm(bestelSysteem);
			ShowLoginForm(form, Functie.Keuken);
		}

        private void buttonManagement_Click(object sender, EventArgs e)
        {
            InlogManagementForm InlogManagementForm = new InlogManagementForm(this, bestelSysteem);
			DialogResult result = InlogManagementForm.ShowDialog();

            if(result == DialogResult.OK){
                this.Hide();
            }
		}

		private void buttonDagoverzicht_Click(object sender, EventArgs e) {
			Dagoverzicht form = new Dagoverzicht(bestelSysteem);
			form.FormClosing += delegate { this.Show(); };
			this.Hide();
			form.Show();

		}
		#endregion
	}
}
