﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bestelsysteem_mayamaya
{
	public class Bestelling
	{
		private string opmerking;
		private bool afgerond;

		/* Read-only properties */
		public int Id { get; private set; }
		public int TafelId { get; private set; }
		public int WerknemerId { get; private set; }
		public double Totaalbedrag { get; private set; }
		public double Fooi { get; private set; }

        /* Read/Write properties */
        public string Opmerking { get { return opmerking; } set { opmerking = value; } }
        public bool Afgerond { get { return afgerond; } set { afgerond = value; } }

		public Bestelling(int id, int tafelId, int werknemerId, bool afgerond, double totaalbedrag, double fooi, string opmerking) {
			this.Id = id;
			this.TafelId = tafelId;
			this.WerknemerId = werknemerId;
			this.afgerond = afgerond;
			this.Totaalbedrag = totaalbedrag;
			this.Fooi = fooi;
			this.opmerking = opmerking;
		}
	}
}
