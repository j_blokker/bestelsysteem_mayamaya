﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bestelsysteem_mayamaya
{
	public class BestelItem
	{
		private int aantal;
		private int gereedgemeld;

		/* Read-only properties */
		public int BestellingId { get; private set; }
		public int MenuItemId { get; private set; }
		public DateTime Opgenomen { get; private set; }

		/* Read/Write properties */
		public int Aantal { get { return aantal; } set { aantal = value; } }
		public int Gereedgemeld { get { return gereedgemeld; } set { gereedgemeld = value; } }

		public BestelItem(int bestellingId, int menuItemId, int aantal, int gereedgemeld, DateTime opgenomen) {
			this.BestellingId = bestellingId;
			this.MenuItemId = menuItemId;
			this.aantal = aantal;
			this.gereedgemeld = gereedgemeld;
			this.Opgenomen = opgenomen;
		}
	}
}
