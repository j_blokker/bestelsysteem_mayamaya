﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bestelsysteem_mayamaya
{
	public class MenuItem
	{
		/* Read-only properties */
		public int Id { get; private set; }
		public int BtwId { get; private set; }
		public int CategorieId { get; private set; }
		public int MenukaartId { get; private set; }
		public string Naam { get; private set; }
		public double Prijs { get; private set; }
		public string Omschrijving { get; private set; }

		public MenuItem(int id, int btwId, int categorieId, int menukaartId, string naam, double prijs, string omschrijving) {
			this.Id = id;
			this.BtwId = btwId;
			this.CategorieId = categorieId;
			this.MenukaartId = menukaartId;
			this.Naam = naam;
			this.Prijs = prijs;
			this.Omschrijving = omschrijving;
		}
	}
}
