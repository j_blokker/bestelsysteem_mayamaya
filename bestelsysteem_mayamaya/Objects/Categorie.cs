﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bestelsysteem_mayamaya
{
	public class Categorie
	{
		/* Read-only properties */
		public int Id { get; private set; }
		public string Naam { get; private set; }

		public Categorie(int id, string naam) {
			this.Id = id;
			this.Naam = naam;
		}

		public override string ToString() {
			return this.Naam;
		}
	}
}
