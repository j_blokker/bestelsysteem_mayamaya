﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bestelsysteem_mayamaya
{
	public class Werknemer
	{
		private string voornaam;
		private string achternaam;
		private string pincode;
		Functie functie;

		/* Read-only properties */
		public int Id { get; private set; }
		
        /* Read/Write properties */
        public string Voornaam { get{return voornaam;} set{voornaam = value;} }
        public string Achternaam { get { return achternaam; } set { achternaam = value; } }
        public string Pincode { get { return pincode; } set { pincode = value; } }
        public Functie Functie { get { return functie; } set { functie = value; } }

		public Werknemer(int id, string voornaam, string achternaam, string pincode, Functie functie) {
			this.Id = id;
			this.voornaam = voornaam;
			this.achternaam = achternaam;
			this.pincode = pincode;
			this.functie = functie;
		}
	}
}
