﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bestelsysteem_mayamaya
{
	public class Tafel
	{
		/* Read-only properties */
		public int Id { get; private set; }
		public int Nummer { get; private set; }
		public int MaxPersonen { get; private set; }
		public bool Bezet { get; private set; }

		public Tafel(int id, int nummer, int maxPersonen, bool bezet) {
			this.Id = id;
			this.Nummer = nummer;
			this.MaxPersonen = maxPersonen;
			this.Bezet = bezet;
		}

		public override string ToString() {
			return "Tafel " + Nummer;
		}
	}
}
