﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
    public partial class OpmerkingInvoegForm : Form
    {
        BestelSysteem bestelSysteem;
        public OpmerkingInvoegForm(BestelSysteem bestelSysteem)
        {
            InitializeComponent();
            this.bestelSysteem = bestelSysteem;
        }

        private void btnAnnuleren_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOpmerkingPlaatsen_Click(object sender, EventArgs e)
        {
            string opmerking = rtbOpmerking.Text;
            //TODO opmerking wegschrijven naar database/lijst

            this.Close();
        }
    }
}
