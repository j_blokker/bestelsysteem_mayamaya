﻿namespace bestelsysteem_mayamaya
{
    partial class OpmerkingInvoegForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbOpmerking = new System.Windows.Forms.RichTextBox();
            this.btnAnnuleren = new System.Windows.Forms.Button();
            this.btnOpmerkingPlaatsen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtbOpmerking
            // 
            this.rtbOpmerking.Location = new System.Drawing.Point(12, 12);
            this.rtbOpmerking.Name = "rtbOpmerking";
            this.rtbOpmerking.Size = new System.Drawing.Size(804, 337);
            this.rtbOpmerking.TabIndex = 0;
            this.rtbOpmerking.Text = "";
            // 
            // btnAnnuleren
            // 
            this.btnAnnuleren.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnuleren.Location = new System.Drawing.Point(257, 376);
            this.btnAnnuleren.Name = "btnAnnuleren";
            this.btnAnnuleren.Size = new System.Drawing.Size(135, 40);
            this.btnAnnuleren.TabIndex = 1;
            this.btnAnnuleren.Text = "Annuleren";
            this.btnAnnuleren.UseVisualStyleBackColor = true;
            this.btnAnnuleren.Click += new System.EventHandler(this.btnAnnuleren_Click);
            // 
            // btnOpmerkingPlaatsen
            // 
            this.btnOpmerkingPlaatsen.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpmerkingPlaatsen.Location = new System.Drawing.Point(435, 376);
            this.btnOpmerkingPlaatsen.Name = "btnOpmerkingPlaatsen";
            this.btnOpmerkingPlaatsen.Size = new System.Drawing.Size(135, 40);
            this.btnOpmerkingPlaatsen.TabIndex = 2;
            this.btnOpmerkingPlaatsen.Text = "OK";
            this.btnOpmerkingPlaatsen.UseVisualStyleBackColor = true;
            this.btnOpmerkingPlaatsen.Click += new System.EventHandler(this.btnOpmerkingPlaatsen_Click);
            // 
            // OpmerkingInvoegForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 449);
            this.ControlBox = false;
            this.Controls.Add(this.btnOpmerkingPlaatsen);
            this.Controls.Add(this.btnAnnuleren);
            this.Controls.Add(this.rtbOpmerking);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OpmerkingInvoegForm";
            this.Text = "Opmerking Invoegen";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbOpmerking;
        private System.Windows.Forms.Button btnAnnuleren;
        private System.Windows.Forms.Button btnOpmerkingPlaatsen;

    }
}