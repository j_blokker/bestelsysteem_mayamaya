﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
    public partial class TafelOverzicht : Form
    {
        BestelSysteem bestelSysteem;

        int tafelNr = 0;

        public TafelOverzicht(BestelSysteem bestelSysteem)
        {
            InitializeComponent();

            this.bestelSysteem = bestelSysteem;
        }
        //naam van werknemer weergeven
        private void TafelOverzicht_Shown(object sender, EventArgs e)
        {
			UpdateSettings();
        }

		public void UpdateSettings() {
			lblIngelogdeMedewerker.Text = bestelSysteem.IngelogdeWerknemer.Voornaam + " " + bestelSysteem.IngelogdeWerknemer.Achternaam;

			DateTime dateTime = DateTime.Now;
			DateTime date = dateTime.Date;

			//loop door alle buttons in panel
			foreach (Control button in this.pnlTafelBtn.Controls) {
				if (button is Button) {
					//haal tafelnummer op uit de knoppen
					bool result = int.TryParse(((Button)button).Text.Replace("Tafel ", ""), out tafelNr);
                    if (!result)
                    {
                        MessageBox.Show("Er is iets fout gegaan met het ophalen van het tafel nummer \n Probeer het nog een keer.");
                    }

					int tafelId = bestelSysteem.ZoekTafelOpTafelNummer(tafelNr).Id;
					int bestellingId = 0;
					foreach (Bestelling bestelling in bestelSysteem.Bestellingen) {
						if (tafelId == bestelling.TafelId && bestelling.Afgerond == false) {
							bestellingId = bestelling.Id;
							break;
						}
					}
					foreach (BestelItem bestelItem in bestelSysteem.BestelItems) {
						if (bestelItem.BestellingId == bestellingId && date == bestelItem.Opgenomen.Date) {
							((Button)button).BackColor = Color.Red;
							break;
						}
					}
				}
			}
		}

        private void btnTafel_Click(object sender, EventArgs e)
        {

            //haal de info van de gedrukte knop op
            Button btnTafel = (Button)sender;
            bool result = int.TryParse(btnTafel.Text.Replace("Tafel ", ""), out tafelNr);
            if (!result)
                    {
                        MessageBox.Show("Er is iets fout gegaan met het ophalen van het tafel nummer \n Probeer het nog een keer.");
                    }
            int tafelId = bestelSysteem.ZoekTafelOpTafelNummer(tafelNr).Id;

            Bestelling bestelling = null;
            foreach (Bestelling item in bestelSysteem.Bestellingen)
            {
                if (tafelId == item.TafelId && item.Afgerond == false)
                {
                    //Er loopt nog een bestelling!
                    bestelling = item;
                }
            }

            if (bestelling == null)
            {
                MessageBox.Show("Er is geen lopende bestelling voor deze tafel gevonden!");
                return;
            }
            BetalingForm BetaalForm = new BetalingForm(tafelNr, bestelSysteem, this);
            BetaalForm.Show();

            this.Hide();

        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            bestelSysteem.Logout();
            this.Close();
        }
    }
}
