﻿namespace bestelsysteem_mayamaya
{
    partial class TafelOverzicht
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TafelOverzicht));
            this.btnLogout = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblIngelogdeMedewerker = new System.Windows.Forms.Label();
            this.pnlTafelBtn = new System.Windows.Forms.Panel();
            this.pnlTafelBtn.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogout
            // 
            this.btnLogout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLogout.BackgroundImage")));
            this.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.btnLogout.Location = new System.Drawing.Point(1184, 14);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(0);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(45, 45);
            this.btnLogout.TabIndex = 18;
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.button9.Location = new System.Drawing.Point(767, 327);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(161, 142);
            this.button9.TabIndex = 7;
            this.button9.Text = "Tafel 9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.btnTafel_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.button6.Location = new System.Drawing.Point(145, 327);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(161, 142);
            this.button6.TabIndex = 8;
            this.button6.Text = "Tafel 6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.btnTafel_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.button3.Location = new System.Drawing.Point(568, 104);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(161, 142);
            this.button3.TabIndex = 9;
            this.button3.Text = "Tafel 3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.btnTafel_Click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.button10.Location = new System.Drawing.Point(973, 327);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(161, 142);
            this.button10.TabIndex = 10;
            this.button10.Text = "Tafel 10";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.btnTafel_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.button8.Location = new System.Drawing.Point(568, 327);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(161, 142);
            this.button8.TabIndex = 11;
            this.button8.Text = "Tafel 8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.btnTafel_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.button7.Location = new System.Drawing.Point(361, 327);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(161, 142);
            this.button7.TabIndex = 12;
            this.button7.Text = "Tafel 7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.btnTafel_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.button5.Location = new System.Drawing.Point(973, 104);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(161, 142);
            this.button5.TabIndex = 13;
            this.button5.Text = "Tafel 5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.btnTafel_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.button4.Location = new System.Drawing.Point(767, 104);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(161, 142);
            this.button4.TabIndex = 14;
            this.button4.Text = "Tafel 4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.btnTafel_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.button2.Location = new System.Drawing.Point(361, 104);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(161, 142);
            this.button2.TabIndex = 15;
            this.button2.Text = "Tafel 2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnTafel_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.button1.Location = new System.Drawing.Point(145, 104);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(161, 142);
            this.button1.TabIndex = 16;
            this.button1.Text = "Tafel 1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnTafel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(105, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 24);
            this.label1.TabIndex = 19;
            this.label1.Text = "Tafeloverzicht";
            // 
            // lblIngelogdeMedewerker
            // 
            this.lblIngelogdeMedewerker.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIngelogdeMedewerker.Location = new System.Drawing.Point(727, 27);
            this.lblIngelogdeMedewerker.Name = "lblIngelogdeMedewerker";
            this.lblIngelogdeMedewerker.Size = new System.Drawing.Size(450, 24);
            this.lblIngelogdeMedewerker.TabIndex = 20;
            this.lblIngelogdeMedewerker.Text = "[Medewerker]";
            this.lblIngelogdeMedewerker.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pnlTafelBtn
            // 
            this.pnlTafelBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlTafelBtn.Controls.Add(this.button1);
            this.pnlTafelBtn.Controls.Add(this.button2);
            this.pnlTafelBtn.Controls.Add(this.button4);
            this.pnlTafelBtn.Controls.Add(this.button9);
            this.pnlTafelBtn.Controls.Add(this.button5);
            this.pnlTafelBtn.Controls.Add(this.button6);
            this.pnlTafelBtn.Controls.Add(this.button7);
            this.pnlTafelBtn.Controls.Add(this.button3);
            this.pnlTafelBtn.Controls.Add(this.button8);
            this.pnlTafelBtn.Controls.Add(this.button10);
            this.pnlTafelBtn.Location = new System.Drawing.Point(-5, 65);
            this.pnlTafelBtn.Name = "pnlTafelBtn";
            this.pnlTafelBtn.Size = new System.Drawing.Size(1272, 600);
            this.pnlTafelBtn.TabIndex = 21;
            // 
            // TafelOverzicht
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 661);
            this.ControlBox = false;
            this.Controls.Add(this.lblIngelogdeMedewerker);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.pnlTafelBtn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TafelOverzicht";
            this.Text = "Bediening";
            this.Shown += new System.EventHandler(this.TafelOverzicht_Shown);
            this.pnlTafelBtn.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblIngelogdeMedewerker;
        private System.Windows.Forms.Panel pnlTafelBtn;
    }
}