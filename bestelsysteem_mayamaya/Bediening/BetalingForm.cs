﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace bestelsysteem_mayamaya
{
    public partial class BetalingForm : Form
    {
		//TODO "Niet alle bestelde menuItems zijn gereedgemeld. Wilt u alsnog de bestelling afronden?" melding indien bestelItems.aantal !== bestelItems.afgerond

        int bestellingId = 0;

        TafelOverzicht tafelOverzicht;
        BestelSysteem bestelSysteem;
        Bestelling bestelling;

        public BetalingForm(int tafelNr, BestelSysteem bestelSysteem, TafelOverzicht tafelOverzicht)
        {
            InitializeComponent();
            this.tafelOverzicht = tafelOverzicht;
            this.bestelSysteem = bestelSysteem;


            lblIngelogdeMedewerker.Text = bestelSysteem.IngelogdeWerknemer.Voornaam + " " + bestelSysteem.IngelogdeWerknemer.Achternaam;

            lbl_tafelNr.Text += " " + tafelNr.ToString();

            Tafel tafel = bestelSysteem.ZoekTafelOpTafelNummer(tafelNr);

            this.bestelling = bestelSysteem.ZoekLopendeBestellingBijTafel(tafel.Id);
            bestellingId = bestelling.Id;

            lblBestellingnr.Text += " " + bestellingId.ToString();


            double totaalPrijs = 0;

            foreach (BestelItem bestelItem in bestelSysteem.BestelItems)
            {
                if(bestelItem.BestellingId == bestellingId){
                    MenuItem mItem = bestelSysteem.ZoekMenuItemOpMenuItemId(bestelItem.MenuItemId);
                    double subPrijs = mItem.Prijs * Convert.ToDouble(bestelItem.Aantal);

                    ListViewItem colomm = new ListViewItem(bestelItem.Aantal.ToString() + "x");
                    colomm.SubItems.Add(mItem.Naam);
                    colomm.SubItems.Add(subPrijs.ToString("c", new CultureInfo("nl-NL")));
                    lvBestelling.Items.Add(colomm);
                    totaalPrijs = totaalPrijs + subPrijs;
                }
            }

            lblTotaalPrijs.Text = totaalPrijs.ToString("c", new CultureInfo("nl-NL"));

        }

        private void btnBonPrinten_Click(object sender, EventArgs e)
        {
            MessageBox.Show("De factuur preview is geprint");
        }

        private void btnOpmerking_Click(object sender, EventArgs e)
        {
            OpmerkingInvoegForm opmerkingForm = new OpmerkingInvoegForm(bestelSysteem);
            opmerkingForm.ShowDialog();
        }

        private void btnBetaalBedrag_Click(object sender, EventArgs e)
        {
            string verschuldigdBedrag = lblTotaalPrijs.Text;
            BetaalBedragForm betaalBedragForm = new BetaalBedragForm(bestelling, bestelSysteem, bestellingId);
            betaalBedragForm.ShowDialog();
        }

        private void btnFactuur_Click(object sender, EventArgs e)
        {
            string totaalVerschuldig = lblTotaalPrijs.Text;
            FactuurForm factuurForm = new FactuurForm(totaalVerschuldig, bestelSysteem, bestellingId, bestelling);
            factuurForm.ShowDialog();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
			tafelOverzicht.UpdateSettings();
			tafelOverzicht.Show();
            this.Close();
        }
    }
}
