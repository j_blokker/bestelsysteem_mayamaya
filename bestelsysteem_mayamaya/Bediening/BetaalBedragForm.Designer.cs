﻿namespace bestelsysteem_mayamaya
{
    partial class BetaalBedragForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbBetaalBedrag = new System.Windows.Forms.TextBox();
            this.lblVerschuldigdBedrag = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblFooi = new System.Windows.Forms.Label();
            this.btnAnnuleren = new System.Windows.Forms.Button();
            this.btnFooiAccept = new System.Windows.Forms.Button();
            this.btnBerekenFooi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(107, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bedrag te betalen: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(107, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Betaald bedrag:";
            // 
            // tbBetaalBedrag
            // 
            this.tbBetaalBedrag.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBetaalBedrag.Location = new System.Drawing.Point(316, 84);
            this.tbBetaalBedrag.Name = "tbBetaalBedrag";
            this.tbBetaalBedrag.Size = new System.Drawing.Size(154, 29);
            this.tbBetaalBedrag.TabIndex = 2;
            this.tbBetaalBedrag.Text = "0,00";
            this.tbBetaalBedrag.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblVerschuldigdBedrag
            // 
            this.lblVerschuldigdBedrag.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVerschuldigdBedrag.Location = new System.Drawing.Point(302, 41);
            this.lblVerschuldigdBedrag.Name = "lblVerschuldigdBedrag";
            this.lblVerschuldigdBedrag.Size = new System.Drawing.Size(168, 24);
            this.lblVerschuldigdBedrag.TabIndex = 3;
            this.lblVerschuldigdBedrag.Text = "123";
            this.lblVerschuldigdBedrag.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(107, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "Totaal aan fooi:";
            // 
            // lblFooi
            // 
            this.lblFooi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFooi.Location = new System.Drawing.Point(333, 145);
            this.lblFooi.Name = "lblFooi";
            this.lblFooi.Size = new System.Drawing.Size(137, 24);
            this.lblFooi.TabIndex = 5;
            this.lblFooi.Text = "0,00";
            this.lblFooi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnAnnuleren
            // 
            this.btnAnnuleren.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnuleren.Location = new System.Drawing.Point(12, 204);
            this.btnAnnuleren.Name = "btnAnnuleren";
            this.btnAnnuleren.Size = new System.Drawing.Size(164, 41);
            this.btnAnnuleren.TabIndex = 6;
            this.btnAnnuleren.Text = "Annuleren";
            this.btnAnnuleren.UseVisualStyleBackColor = true;
            this.btnAnnuleren.Click += new System.EventHandler(this.btnAnnuleren_Click);
            // 
            // btnFooiAccept
            // 
            this.btnFooiAccept.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFooiAccept.Location = new System.Drawing.Point(419, 204);
            this.btnFooiAccept.Name = "btnFooiAccept";
            this.btnFooiAccept.Size = new System.Drawing.Size(164, 41);
            this.btnFooiAccept.TabIndex = 7;
            this.btnFooiAccept.Text = "OK";
            this.btnFooiAccept.UseVisualStyleBackColor = true;
            this.btnFooiAccept.Click += new System.EventHandler(this.btnFooiAccept_Click);
            // 
            // btnBerekenFooi
            // 
            this.btnBerekenFooi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBerekenFooi.Location = new System.Drawing.Point(217, 204);
            this.btnBerekenFooi.Name = "btnBerekenFooi";
            this.btnBerekenFooi.Size = new System.Drawing.Size(164, 41);
            this.btnBerekenFooi.TabIndex = 8;
            this.btnBerekenFooi.Text = "Bereken fooi";
            this.btnBerekenFooi.UseVisualStyleBackColor = true;
            this.btnBerekenFooi.Click += new System.EventHandler(this.btnBerekenFooi_Click);
            // 
            // BetaalBedragForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 293);
            this.Controls.Add(this.btnBerekenFooi);
            this.Controls.Add(this.btnFooiAccept);
            this.Controls.Add(this.btnAnnuleren);
            this.Controls.Add(this.lblFooi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblVerschuldigdBedrag);
            this.Controls.Add(this.tbBetaalBedrag);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "BetaalBedragForm";
            this.Text = "Betaal Bedrag";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbBetaalBedrag;
        private System.Windows.Forms.Label lblVerschuldigdBedrag;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblFooi;
        private System.Windows.Forms.Button btnAnnuleren;
        private System.Windows.Forms.Button btnFooiAccept;
        private System.Windows.Forms.Button btnBerekenFooi;
    }
}