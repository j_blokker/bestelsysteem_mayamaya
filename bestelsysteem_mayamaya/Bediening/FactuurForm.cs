﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bestelsysteem_mayamaya
{
    public partial class FactuurForm : Form
    {
        BestelSysteem bestelSysteem;
        int bestellingId;
        Bestelling bestelling;

        public FactuurForm(string totaalVerschuldig, BestelSysteem bestelSysteem, int bestellingId, Bestelling bestelling)
        {
            this.bestellingId = bestellingId;
            this.bestelling = bestelling;
            this.bestelSysteem = bestelSysteem;

            InitializeComponent();

            lblTotaalVerschuldig.Text = totaalVerschuldig;
        }

        private void btnAnnuleren_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnBetaald_Click(object sender, EventArgs e)
        {
            bestelSysteem.BetalenBestellingFactuur(bestelling, bestellingId);
            this.Close();
        }
        private void btnBetaaldMetBon_Click(object sender, EventArgs e)
        {
            MessageBox.Show("De bon is uitgeprint");
            bestelSysteem.BetalenBestellingFactuur(bestelling, bestellingId);
            this.Close();
        }
    }
}
