﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace bestelsysteem_mayamaya
{
    public partial class BetaalBedragForm : Form
    {
        double totaalPrijs = 0;
        public BetaalBedragForm(Bestelling bestelling, BestelSysteem bestelSysteem, int bestellingId)
        {
            InitializeComponent();
            //registreer key aanslagen
            this.tbBetaalBedrag.KeyPress += new System.Windows.Forms.KeyPressEventHandler(CheckKeys);
            
            foreach (BestelItem bestelItem in bestelSysteem.BestelItems)
            {
                if(bestelItem.BestellingId == bestellingId){
                    MenuItem mItem = bestelSysteem.ZoekMenuItemOpMenuItemId(bestelItem.MenuItemId);
                    double subPrijs = mItem.Prijs * Convert.ToDouble(bestelItem.Aantal);
                    totaalPrijs = totaalPrijs + subPrijs;
                }
            }
            //totaal prijs tonen
            lblVerschuldigdBedrag.Text = totaalPrijs.ToString();
        }

        private void btnFooiAccept_Click(object sender, EventArgs e)
        {
            //TODO fooi toevoegen aan listview en totaalprijs
            this.Close();
        }

        private void btnAnnuleren_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BerekenFooi(double verschuldigdBedrag, string betaaldBedrag)
        {
            double bedrag = 0;
            //De komma omzetten naar punt (voor de double conversie)
            string opgegevenBedrag = betaaldBedrag;
            opgegevenBedrag = opgegevenBedrag.Replace(",", ".");

            bool result = double.TryParse(opgegevenBedrag, out bedrag);
            if (result)
            {
                if (verschuldigdBedrag <= bedrag)
                {
                    //bereken de fooi
                    lblFooi.Text = (bedrag - verschuldigdBedrag).ToString("c", new CultureInfo("nl-NL"));
                }
                else
                {
                    MessageBox.Show("Controleer het ingevulde bedrag. Dit bedrag moet groter of gelijk zijn aan het verschuldigde bedrag.");
                }
            }
            else {
                MessageBox.Show("Vul een geldig bedrag in");
            }
        }

        private void btnBerekenFooi_Click(object sender, EventArgs e)
        {

            BerekenFooi(totaalPrijs, tbBetaalBedrag.Text);
        }

        private void CheckKeys(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            //check of de enter key is ingedrukt
            if (e.KeyChar == (char)13)
            {
                BerekenFooi(totaalPrijs, tbBetaalBedrag.Text);
            }
        }
    }
}
