﻿namespace bestelsysteem_mayamaya
{
    partial class BetalingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BetalingForm));
            this.btnBonPrinten = new System.Windows.Forms.Button();
            this.btnBetaalBedrag = new System.Windows.Forms.Button();
            this.btnFactuur = new System.Windows.Forms.Button();
            this.btnOpmerking = new System.Windows.Forms.Button();
            this.lbl_tafelNr = new System.Windows.Forms.Label();
            this.lblIngelogdeMedewerker = new System.Windows.Forms.Label();
            this.btnHome = new System.Windows.Forms.Button();
            this.lblBestellingnr = new System.Windows.Forms.Label();
            this.lvBestelling = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTotaalPrijs = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBonPrinten
            // 
            this.btnBonPrinten.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBonPrinten.Location = new System.Drawing.Point(58, 151);
            this.btnBonPrinten.Name = "btnBonPrinten";
            this.btnBonPrinten.Size = new System.Drawing.Size(502, 69);
            this.btnBonPrinten.TabIndex = 4;
            this.btnBonPrinten.Text = "Preview bon printen";
            this.btnBonPrinten.UseVisualStyleBackColor = true;
            this.btnBonPrinten.Click += new System.EventHandler(this.btnBonPrinten_Click);
            // 
            // btnBetaalBedrag
            // 
            this.btnBetaalBedrag.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBetaalBedrag.Location = new System.Drawing.Point(58, 254);
            this.btnBetaalBedrag.Name = "btnBetaalBedrag";
            this.btnBetaalBedrag.Size = new System.Drawing.Size(502, 69);
            this.btnBetaalBedrag.TabIndex = 3;
            this.btnBetaalBedrag.Text = "Betaal bedrag invoeren";
            this.btnBetaalBedrag.UseVisualStyleBackColor = true;
            this.btnBetaalBedrag.Click += new System.EventHandler(this.btnBetaalBedrag_Click);
            // 
            // btnFactuur
            // 
            this.btnFactuur.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFactuur.Location = new System.Drawing.Point(58, 357);
            this.btnFactuur.Name = "btnFactuur";
            this.btnFactuur.Size = new System.Drawing.Size(502, 69);
            this.btnFactuur.TabIndex = 2;
            this.btnFactuur.Text = "Factuur aanmaken";
            this.btnFactuur.UseVisualStyleBackColor = true;
            this.btnFactuur.Click += new System.EventHandler(this.btnFactuur_Click);
            // 
            // btnOpmerking
            // 
            this.btnOpmerking.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpmerking.Location = new System.Drawing.Point(58, 460);
            this.btnOpmerking.Name = "btnOpmerking";
            this.btnOpmerking.Size = new System.Drawing.Size(502, 69);
            this.btnOpmerking.TabIndex = 1;
            this.btnOpmerking.Text = "Opmerking toevoegen";
            this.btnOpmerking.UseVisualStyleBackColor = true;
            this.btnOpmerking.Click += new System.EventHandler(this.btnOpmerking_Click);
            // 
            // lbl_tafelNr
            // 
            this.lbl_tafelNr.AutoSize = true;
            this.lbl_tafelNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_tafelNr.Location = new System.Drawing.Point(63, 74);
            this.lbl_tafelNr.Name = "lbl_tafelNr";
            this.lbl_tafelNr.Size = new System.Drawing.Size(51, 24);
            this.lbl_tafelNr.TabIndex = 5;
            this.lbl_tafelNr.Text = "Tafel";
            // 
            // lblIngelogdeMedewerker
            // 
            this.lblIngelogdeMedewerker.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblIngelogdeMedewerker.Location = new System.Drawing.Point(809, 20);
            this.lblIngelogdeMedewerker.Name = "lblIngelogdeMedewerker";
            this.lblIngelogdeMedewerker.Size = new System.Drawing.Size(352, 24);
            this.lblIngelogdeMedewerker.TabIndex = 18;
            this.lblIngelogdeMedewerker.Text = "Medewerker:   [Medewerker]";
            this.lblIngelogdeMedewerker.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnHome
            // 
            this.btnHome.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHome.BackgroundImage")));
            this.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHome.Location = new System.Drawing.Point(1186, 14);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(49, 42);
            this.btnHome.TabIndex = 19;
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // lblBestellingnr
            // 
            this.lblBestellingnr.AutoSize = true;
            this.lblBestellingnr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBestellingnr.Location = new System.Drawing.Point(880, 74);
            this.lblBestellingnr.Name = "lblBestellingnr";
            this.lblBestellingnr.Size = new System.Drawing.Size(91, 24);
            this.lblBestellingnr.TabIndex = 20;
            this.lblBestellingnr.Text = "Bestelling";
            // 
            // lvBestelling
            // 
            this.lvBestelling.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvBestelling.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvBestelling.Location = new System.Drawing.Point(640, 151);
            this.lvBestelling.Name = "lvBestelling";
            this.lvBestelling.Size = new System.Drawing.Size(575, 415);
            this.lvBestelling.TabIndex = 21;
            this.lvBestelling.UseCompatibleStateImageBehavior = false;
            this.lvBestelling.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Aantal";
            this.columnHeader1.Width = 108;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Naam";
            this.columnHeader2.Width = 360;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Prijs";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader3.Width = 87;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(414, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 24);
            this.label4.TabIndex = 29;
            this.label4.Text = "Totaal:";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblTotaalPrijs);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(640, 566);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(575, 37);
            this.panel1.TabIndex = 22;
            // 
            // lblTotaalPrijs
            // 
            this.lblTotaalPrijs.AutoSize = true;
            this.lblTotaalPrijs.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotaalPrijs.Location = new System.Drawing.Point(503, 6);
            this.lblTotaalPrijs.Name = "lblTotaalPrijs";
            this.lblTotaalPrijs.Size = new System.Drawing.Size(65, 24);
            this.lblTotaalPrijs.TabIndex = 30;
            this.lblTotaalPrijs.Text = "123,45";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(12, 101);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1240, 1);
            this.panel3.TabIndex = 24;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Location = new System.Drawing.Point(12, 70);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1240, 1);
            this.panel4.TabIndex = 25;
            // 
            // BetalingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 661);
            this.ControlBox = false;
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lvBestelling);
            this.Controls.Add(this.lblBestellingnr);
            this.Controls.Add(this.btnHome);
            this.Controls.Add(this.lblIngelogdeMedewerker);
            this.Controls.Add(this.lbl_tafelNr);
            this.Controls.Add(this.btnOpmerking);
            this.Controls.Add(this.btnFactuur);
            this.Controls.Add(this.btnBetaalBedrag);
            this.Controls.Add(this.btnBonPrinten);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BetalingForm";
            this.Text = "Betaling";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBonPrinten;
        private System.Windows.Forms.Button btnBetaalBedrag;
        private System.Windows.Forms.Button btnFactuur;
        private System.Windows.Forms.Button btnOpmerking;
        private System.Windows.Forms.Label lbl_tafelNr;
        private System.Windows.Forms.Label lblIngelogdeMedewerker;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Label lblBestellingnr;
        private System.Windows.Forms.ListView lvBestelling;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblTotaalPrijs;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;

    }
}